/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 */

#ifndef __ISP_DMA_V4L2_H__
#define __ISP_DMA_V4L2_H__

#include <linux/dmaengine.h>
#include <linux/mutex.h>
#include <linux/spinlock.h>
#include <linux/videodev2.h>

#include <media/media-entity.h>
#include <media/v4l2-device.h>
#include <media/v4l2-mem2mem.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-dev.h>
#include <media/videobuf2-v4l2.h>

/* Forward declarations */
struct dma_chan;
struct isp_dma_format;

/**
 * isp_dma_type - ISP DMA types
 */
enum isp_dma_type {
	ISP_DMA_TYPE_CAPTURE = 0, // Write DMA instances (DEV_TO_MEM)
	ISP_DMA_TYPE_OUTPUT = 1, // Read DMA instance (MEM_TO_DEV)
	ISP_DMA_TYPE_NUM = 2
};

/**
 * struct isp_video_format - ISP Video IP video format description
 * @vf_code: AXI4 video format code
 * @width: AXI4 format width in bits per component
 * @pattern: CFA pattern for Mono/Sensor formats
 * @code: media bus format code
 * @bpl_factor: Bytes per line factor
 * @bpp: bits per pixel
 * @fourcc: V4L2 pixel format FCC identifier
 * @num_planes: number of planes w.r.t. color format
 * @buffers: number of buffers per format
 * @hsub: Horizontal sampling factor of Chroma
 * @vsub: Vertical sampling factor of Chroma
 */
struct isp_video_format {
	unsigned int vf_code;
	unsigned int width;
	const char *pattern;
	unsigned int code;
	unsigned int bpl_factor;
	unsigned int bpp;
	u32 fourcc;
	u8 num_planes;
	u8 buffers;
	u8 hsub;
	u8 vsub;
};

/**
 * struct isp_dma - Video DMA channel
 * @dev: Handle of parent device.
 * @video_dev: V4L2 video device associated with the DMA channel
 * @v4l2_dev: V4L2 device driver which is registered in video_dev.
 * @v4l2_m2m_dev: V4L2 memory-to-memory device, only used in M2M mode.
 * @type: DMA type: Capture or Output.
 * @channels: DMA engine channels.
 * @pad: Media pad for the video device entity.
 * @format_supported: Array that is allocated and filled during probing.
 *                    Contains "true" for each format that is supported
 *                    by the device connected to that pad.
 * @queue: vb2 buffer queue which is registered in video_dev in case
 *         device operates in capture or output mode (not m2m).
 * @lock: Protects the @format, @fmtinfo and @queue fields.
 * @format: Active V4L2 pixel format.
 * @dma_format: Pointer to active ISP DMA format.
 * @pipe: Pointer to media pipeline.
 * @start_stream: Function used to start or stop streaming in the @pipe.
 * @bridge_dev: composite device the DMA channel belongs to
 * @pipe: pipeline belonging to the DMA channel
 * @queued_bufs: list of queued buffers
 * @queued_lock: protects the buf_queued list
 */
struct isp_dma {
	struct device *dev;
	struct video_device video_dev;
	struct v4l2_device *v4l2_dev;
	struct v4l2_m2m_dev *m2m_dev;

	enum isp_dma_type type;
	struct dma_chan *channel;
	struct media_pad pad;
	bool *format_supported;

	// Video format description and buffer queue
	struct mutex lock;
	struct vb2_queue queue;
	struct v4l2_format format;
	const struct isp_dma_format *dma_format;
	size_t metadata_buffersize;

	// Media pipeline handling
	struct media_pipeline *pipe;

	// Buffer queue
	int (*start_stream)(struct device *, bool);
	void (*transfer_done)(struct device *, struct vb2_v4l2_buffer *);
	struct list_head queued_bufs;
	spinlock_t queued_lock;
	u32 sequence;
};

/**
 * isp_dma_init - Initialize the given DMA device
 * @dev: Handle of the overlying device which provides the start_stream
 *       function.
 * @dma: ISP DMA structure that shall be initialized. It is expected that
 *       this struct has been zeroed before calling this function (usually
 *       during allocation).
 * @channel_name: Name of the DMA channel (from device tree). The DMA type
 *                (capture or output) will be fetched automatically from the
 *                DMA channel.
 * @v4l2_dev: Pointer to a V4L2 video device that will be associated with
              the video device node.
 * @pipe: Media pipeline the ISP DMA is part of.
 * @start_stream: Function used to indicate streaming in the pipeline is started
 *                or stopped.
 *
 * Return: 0 on success, negative error code otherwise
 *
 * At least one of capture_channel_name and output_channel_name must be given,
 * the other one may be NULL. If both are set an M2M device is created.
 */
int isp_dma_init(struct device *dev, struct isp_dma *dma,
		 const char *channel_name, struct v4l2_device *v4l2_dev,
		 struct media_pipeline *pipe,
		 int (*start_stream)(struct device *, bool),
		 void (*transfer_done)(struct device *,
				       struct vb2_v4l2_buffer *v4l2_buf));

/**
 * isp_dma_cleanup - Clean up ISP DMA device
 *
 * @param dma ISP DMA structure that shall be cleaned up.
 */
void isp_dma_cleanup(struct isp_dma *dma);

/**
 * isp_dma_set_supported_formats - Tell DMA driver which formats are supported
 * @param dma Pointer to ISP DMA instance.
 * @param fourcc An array of fourcc codes of the supported fromats of size
 *               "len".
 * @param len Length of the fourcc array (number of supported formats).
 *
 * Return: 0 on success,
 *         -EINVAL if the fourcc list contains a format that is not known by the
 *                 DMA.
 *
 * Loops over all formats that are supported by the DMA and checks if
 * they are also supported by the connected device.
 *
 * If this function is not called the DMA will assume all formats are
 * supported.
 */
int isp_dma_set_supported_formats(struct isp_dma *dma, u32 *fourcc, size_t len);

/**
 * isp_dma_set_supported_formats - Tell DMA driver which formats are supported
 * @param dma Pointer to ISP DMA instance.
 * @param format Pointer to a V4L2 format structure
 *
 * Return: 0 on success, error code on failure
 */
int isp_dma_override_format(struct isp_dma *dma, struct v4l2_format *format);

/**
 * isp_dma_get_dma_mask - Get DMA mask of DMA device
 * @param dma Pointer to ISP DMA instance.
 *
 * Return: DMA mask of the ISP DMA.
 *
 * Can be used to retrieve the maximum DMA address that the DMA can handle.
 */
u64 isp_dma_get_dma_mask(struct isp_dma *dma);

/**
 * isp_dma_set_metadata - Set type and buffersize for metadata channels
 * @param dma Pointer to ISP DMA instance
 * @param dataformat   see V4L2_META_FMT_xxx
 * @param buffersize   metadata buffer size
 * @param transfersize metadata transfer size (transfersize < buffersize)
 *
 * Return: 0 on success,
 *         -EINVAL if the dma channel is not a metadata channel
 */
int isp_dma_set_metadata(struct isp_dma *dma, u32 dataformat, u32 buffersize,
			 u32 transfersize);

/**
 * isp_dma_disable_transfer - Disable all transfers
 * @param chan Pointer to dma channel
 */
void isp_dma_disable_transfer(struct dma_chan *chan);

/**
 * isp_dma_reenable_transfer - Re enable transfers after @isp_dma_disable_transfer
 * @param chan Pointer to dma channel
 */
void isp_dma_reenable_transfer(struct dma_chan *chan);

#endif /* __ISP_DMA_V4L2_H__ */
