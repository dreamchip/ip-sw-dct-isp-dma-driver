/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 */

//#define DEBUG

#include <linux/delay.h>
#include <linux/dma-mapping.h>
#include <linux/interrupt.h>
#include <linux/of_address.h>
#include <linux/of_dma.h>
#include <linux/of_irq.h>
#include <linux/of_platform.h>
#include <linux/timer.h>
#include <linux/clk.h>
#include <linux/debugfs.h>

#include "dmaengine.h"

#include "../common/regs/dct_reg_helper.h"
#include "../common/regs/rpp_dma_read_regs.h"
#include "../common/regs/rpp_dma_write_regs.h"

#include "isp-dma.h"

/**
 * struct isp_dma_tx_descriptor - Per transaction descriptor structure
 * @common: Async transaction descriptor.
 * @cfg: Channel configuration which will be used for this transfers.
 * @cfg_id: ID of the above channel configuration, used to check if config
 *          was updated since last transfer.
 * @plane_addresses: DMA addresses for the planes in memory.
 * @node: list_head for descriptor lists.
 */
struct isp_dma_tx_descriptor {
	struct dma_async_tx_descriptor common;
	struct isp_dma_channel_config cfg;
	u16 cfg_id;
	dma_addr_t plane_addresses[ISP_DMA_NUM_PLANES];
	struct list_head node;
};

/**
 * struct isp_dma_stats - ISP DMA channel statistics
 *
 * @transfer_done: Counts the number of received transfer done interrupts
 * @overrun: Counts the number of overruns occured
 * @no_active_desc: Counts the number activations without an activ descriptor
 */
struct isp_dma_stats {
	int transfer_done;
	int config_done;
	int overrun;
	int no_active_desc;
};

#define isp_dma_addr(base, offset) ((void *)((uintptr_t)base + offset))
#define isp_dma_writel(base, offset, value) \
	writel(value, isp_dma_addr(base, offset))
#define isp_dma_readl(base, offset) readl(isp_dma_addr(base, offset))

/**
 * struct isp_dma_channel - ISP DMA channel structure
 * @common: Common DMA channel struture.
 * @regs: IO-mapped DMA channel register interface.
 * @irq_transfer_done: Transfer done interrupt.
 * @irq_config_done: Shadow update done interrupt (ready for next config).
 * @direction: Transfer direction (DMA_DEV_TO_MEM or DMA_MEM_TO_DEV).
 * @video_dev_nr: Number that can be used to enumarate a video device that is
 *                created for this DMA channel. Is not used internally in this
 *                driver but can be used by high-level drivers (e.g. V4L2
 *                drivers). Read from device tree during probing.
 * @cfg: Channel configuration which will be used for new transfers.
 * @cfg_id: ID of the above channel configuration, incremented when config is updated.
 *          An ID of 0 indicates an invalid configuration (has not been set yet), so
 *          it must be avoided when the ID overflows.
 * @cfg_id_active: ID of the last programmed configuration which is currently active.
 *                 Used to detect if the configuration got changed and registers must
 *                 be programmed with new settings.
 * @cfg_lock: Lock that protects access to all cfg_* members of this struct.
 *            The configuration is not used from interrupt context, so regular
 *            spin_lock() and spin_unlock() can be used.
 * @desc_pending_list: Descriptors waiting to be issued.
 * @desc_issued_list: Descriptors waiting to be processed. The first entry in
 *                    the list will be moved to desc_next when a new transfer
 *                    is started.
 * @desc_next: Pointer to descriptor which is transfered next and whose config is
 *             currently been written or has already been written to the shadow
 *             registers.
 * @desc_active: Pointer to descriptor which is currently being processed by the DMA.
 * @desc_done_list: Completed descriptors which are waiting to be cleaned up by
 *                  the tasklet.
 * @desc_lock: Lock that protects all desc_* members of this struct. Is locked from both
 *             threads and interrupt context.
 * @tasklet_cleanup: Cleanup work after transfer done.
 * @timer_restart: Restart DMA in case config done interrupt was missed.
 * @terminating: Set if the client has requested to terminate all transfers.
 */
struct isp_dma_channel {
	struct dma_chan common;
	uint32_t __iomem *regs;
	int irq_transfer_done;
	int irq_config_done;
	enum dma_transfer_direction direction;
	int video_dev_nr;
	bool metadata;
	bool direct_mode;
	struct isp_dma_channel_config cfg;
	u16 cfg_id;
	u16 cfg_id_active;
	spinlock_t cfg_lock;
	struct list_head desc_pending_list;
	struct list_head desc_issued_list;
	struct isp_dma_tx_descriptor *desc_next;
	struct isp_dma_tx_descriptor *desc_active;
	struct list_head desc_done_list;
	spinlock_t desc_lock;
	struct tasklet_struct tasklet_cleanup;
	struct timer_list timer_restart;
	struct isp_dma_stats dma_stats;
	bool terminating;

	struct platform_device *direct_source;
	dma_addr_t direct_mode_addr[ISP_DMA_NUM_PLANES];
	// @todo leaky was introduced for testing trying to avoid an overflow
	// of the acq fifo in isp_pre. Remove as soon as investigations are complete.
	bool leaky_mem_avail;
	dma_addr_t leaky_mode_addr_dma[ISP_DMA_NUM_PLANES];
	void *leaky_mode_addr_cpu[ISP_DMA_NUM_PLANES];
};

/* Macros */
#define to_isp_dma_channel(chan) \
	container_of(chan, struct isp_dma_channel, common)
#define to_isp_dma_tx_descriptor(desc) \
	container_of(desc, struct isp_dma_tx_descriptor, common)

#define IS_WR_CHAN(chan) (chan->direction == DMA_DEV_TO_MEM)
#define IS_RD_CHAN(chan) (chan->direction == DMA_MEM_TO_DEV)

static void isp_dma_soft_reset(struct isp_dma_channel *isp_chan);
static void isp_dma_config_update(struct isp_dma_channel *isp_chan, bool force);

// desc_next and desc_active need to be "occupied" when using leaky_buffer
// in order to synchronize with newly queued buffers correctly.
// The address to this dummy descriptor id used for that. Can be removed
// with leaky buffer workaround.
static struct isp_dma_tx_descriptor desc_leaky = { 0 };

/* -----------------------------------------------------------------------------
 * Public API
 */

enum dma_transfer_direction
isp_dma_get_transfer_direction(struct dma_chan *chan)
{
	return to_isp_dma_channel(chan)->direction;
}
EXPORT_SYMBOL(isp_dma_get_transfer_direction);

int isp_dma_get_video_device_number(struct dma_chan *chan)
{
	return to_isp_dma_channel(chan)->video_dev_nr;
}
EXPORT_SYMBOL(isp_dma_get_video_device_number);

bool isp_dma_is_metadata_channel(struct dma_chan *chan)
{
	return to_isp_dma_channel(chan)->metadata;
}
EXPORT_SYMBOL(isp_dma_is_metadata_channel);

bool isp_dma_is_direct_mode(struct dma_chan *chan)
{
	return to_isp_dma_channel(chan)->direct_mode;
}
EXPORT_SYMBOL(isp_dma_is_direct_mode);

struct platform_device *isp_dma_get_direct_source(struct dma_chan *chan)
{
	return to_isp_dma_channel(chan)->direct_source;
}
EXPORT_SYMBOL(isp_dma_get_direct_source);

void isp_dma_disable_transfer(struct dma_chan *chan)
{
	struct isp_dma_channel *isp_chan = to_isp_dma_channel(chan);
	uint32_t reg = isp_dma_readl(isp_chan->regs, DMA_READ_CFG_REG);

	DCT_REGS_FLAG_CLEAR(reg, DMA_READ_ENABLE0);
	DCT_REGS_FLAG_CLEAR(reg, DMA_READ_ENABLE1);
	DCT_REGS_FLAG_CLEAR(reg, DMA_READ_ENABLE2);
	DCT_REGS_FLAG_CLEAR(reg, DMA_READ_ENABLE3);
	isp_dma_writel(isp_chan->regs, DMA_READ_CFG_REG, reg);

	isp_dma_config_update(isp_chan, true);
	isp_dma_soft_reset(isp_chan);
}
EXPORT_SYMBOL(isp_dma_disable_transfer);

void isp_dma_reenable_transfer(struct dma_chan *chan)
{
	struct isp_dma_channel *isp_chan = to_isp_dma_channel(chan);
	struct isp_dma_channel_config *cfg = &isp_chan->cfg;
	uint32_t reg = isp_dma_readl(isp_chan->regs, DMA_READ_CFG_REG);

	DCT_REGS_FLAG_SET(reg, DMA_READ_ENABLE0);
	DCT_REGS_FLAG_ASSIGN(reg, DMA_READ_ENABLE1,
			     cfg->active_planes >= 2 ? !cfg->planes[1].dummy :
						       0);
	DCT_REGS_FLAG_ASSIGN(reg, DMA_READ_ENABLE2,
			     cfg->active_planes >= 3 ? !cfg->planes[2].dummy :
						       0);
	DCT_REGS_FLAG_ASSIGN(reg, DMA_READ_ENABLE3,
			     cfg->active_planes >= 4 ? !cfg->planes[3].dummy :
						       0);
	isp_dma_writel(isp_chan->regs, DMA_READ_CFG_REG, reg);

	isp_dma_config_update(isp_chan, true);
}
EXPORT_SYMBOL(isp_dma_reenable_transfer);

/* -----------------------------------------------------------------------------
 * Program registers
 */

/**
 * isp_dma_soft_reset- Do a soft reset of the DMA channel
 *
 * @chan: Pointer to ISP DMA channel
 *
 * This will stop the current transfer immediately
 */
static void isp_dma_soft_reset(struct isp_dma_channel *isp_chan)
{
	uint32_t reg = isp_dma_readl(isp_chan->regs, DMA_READ_CTRL_REG);
	DCT_REGS_FLAG_SET(reg, SOFT_RESET);
	isp_dma_writel(isp_chan->regs, DMA_READ_CTRL_REG, reg);
	DCT_REGS_FLAG_CLEAR(reg, SOFT_RESET);
	isp_dma_writel(isp_chan->regs, DMA_READ_CTRL_REG, reg);
}

/**
 * isp_dma_config_update - Trigger immediate or automatic config update
 *
 * @chan: Pointer to ISP DMA channel
 * @force: If update shall be done immediately (true) or at frame end (false)
 *
 * All settings of the ISP DMA are written to shadow registers and do not
 * directly affect the current operation. To apply settings a config update
 * has to be triggered. This cann be done immediately (forced) or automatic
 * at frame end.
 *
 * An immediate update should only be performed when the DMA is idle, otherwise
 * the current transfer will be aborted.
 */
static void isp_dma_config_update(struct isp_dma_channel *isp_chan, bool force)
{
	uint32_t reg = isp_dma_readl(isp_chan->regs, DMA_READ_CTRL_REG);
	if (force) {
		/* Perform immediate config update */
		DCT_REGS_FLAG_SET(reg, DMA_READ_CFG_UPD);
	} else {
		/* Generate config update at transfer end */
		DCT_REGS_FLAG_SET(reg, DMA_READ_GEN_CFG_UPD);
	}
	isp_dma_writel(isp_chan->regs, DMA_READ_CTRL_REG, reg);
}

/**
 * isp_dma_program_transfer - Program shadow registers for next transfer
 * @chan: Pointer to ISP DMA channel
 * @desc: Descriptor for next DMA transfer
 */
static void isp_dma_program_transfer(struct isp_dma_channel *isp_chan,
				     struct isp_dma_tx_descriptor *desc)
{
	struct isp_dma_channel_config *cfg = desc ? &desc->cfg : &isp_chan->cfg;
	dma_addr_t addr[ISP_DMA_NUM_PLANES];
	int source_index = 0;
	u32 reg;

	/* Note: There is no need to perform error checks on the configuration
	 * since that has already been done in isp_dma_device_config() and
	 * isp_dma_prep_slave_sg(). */

	/* Update channel configuration registers if config has changed */
	if (!desc || (desc->cfg_id != isp_chan->cfg_id_active)) {
		if (desc)
			isp_chan->cfg_id_active = desc->cfg_id;
		// General configuration
		reg = 0;
		DCT_REGS_FIELD_SET(reg, CFG_NUM_PLANES, cfg->active_planes);
		DCT_REGS_FLAG_ASSIGN(reg, DMA_READ_MULTI_CHANNEL_MODE,
				     cfg->multi_lane);
		DCT_REGS_FLAG_ASSIGN(reg, DMA_READ_420_MODE, cfg->mode_420);
		DCT_REGS_FLAG_ASSIGN(reg, DMA_READ_ENABLE0,
				     !cfg->planes[0].dummy);
		DCT_REGS_FLAG_ASSIGN(
			reg, DMA_READ_ENABLE1,
			cfg->active_planes >= 2 ? !cfg->planes[1].dummy : 0);
		DCT_REGS_FLAG_ASSIGN(
			reg, DMA_READ_ENABLE2,
			cfg->active_planes >= 3 ? !cfg->planes[2].dummy : 0);
		DCT_REGS_FLAG_ASSIGN(
			reg, DMA_READ_ENABLE3,
			cfg->active_planes >= 4 ? !cfg->planes[3].dummy : 0);

		DCT_REGS_FLAG_ASSIGN(reg, DMA_READ_AXI_SLAVE_MODE,
				     isp_chan->direct_mode);
		// "Unknown Size Mode" is only available for write channels
		// Enable it for metadata channels
		if (IS_WR_CHAN(isp_chan))
			DCT_REGS_FLAG_ASSIGN(reg, DMA_WRITE_UNKNOWN_SIZE_MODE,
				isp_chan->metadata);
		isp_dma_writel(isp_chan->regs, DMA_READ_CFG_REG, reg);

		// Input component sizes
		reg = 0;
		DCT_REGS_FIELD_SET(reg, DMA_READ_PXL_WIDTH0,
				   cfg->planes[0].comp_size);
		DCT_REGS_FIELD_SET(reg, DMA_READ_PXL_WIDTH1,
				   cfg->planes[1].comp_size);
		DCT_REGS_FIELD_SET(reg, DMA_READ_PXL_WIDTH2,
				   cfg->planes[2].comp_size);
		DCT_REGS_FIELD_SET(reg, DMA_READ_PXL_WIDTH3,
				   cfg->planes[3].comp_size);
		isp_dma_writel(isp_chan->regs, DMA_READ_CFG_IN_SIZE_REG, reg);
		// The stride is divided by 256 which is the required alignment.
		reg = 0;
		DCT_REGS_FIELD_SET(reg, DMA_READ_MEM_LINE_SIZE0,
				   cfg->planes[0].stride >> 8);
		isp_dma_writel(isp_chan->regs, DMA_READ_MEM_LINE_SIZE0_REG,
			       reg);
		reg = 0;
		DCT_REGS_FIELD_SET(reg, DMA_READ_MEM_LINE_SIZE1,
				   cfg->planes[1].stride >> 8);
		isp_dma_writel(isp_chan->regs, DMA_READ_MEM_LINE_SIZE1_REG,
			       reg);
		reg = 0;
		DCT_REGS_FIELD_SET(reg, DMA_READ_MEM_LINE_SIZE2,
				   cfg->planes[2].stride >> 8);
		isp_dma_writel(isp_chan->regs, DMA_READ_MEM_LINE_SIZE2_REG,
			       reg);
		reg = 0;
		DCT_REGS_FIELD_SET(reg, DMA_READ_MEM_LINE_SIZE3,
				   cfg->planes[3].stride >> 8);
		isp_dma_writel(isp_chan->regs, DMA_READ_MEM_LINE_SIZE3_REG,
			       reg);
		// Image dimensions
		reg = 0;
		DCT_REGS_FIELD_SET(reg, DMA_READ_H_SIZE, cfg->planes[0].width);
		DCT_REGS_FIELD_SET(reg, DMA_READ_V_SIZE, cfg->planes[0].height);
		isp_dma_writel(isp_chan->regs, DMA_READ_PIC_SIZE_REG, reg);
		/* Read channels have only one "pic_size" register, therefore all planes
		 * must have identical dimensions. For write channels they can be different: */
		if (IS_WR_CHAN(isp_chan)) {
			/* In case of 4:2:0 mode the image dimensions for the second plane
			 * are set identical to the first plane as the HW works on the full
			 * image dimensions internally but skips even lines. */
			reg = 0;
			DCT_REGS_FIELD_SET(
				reg, DMA_WRITE_H_SIZE1,
				cfg->planes[cfg->mode_420 ? 0 : 1].width);
			DCT_REGS_FIELD_SET(
				reg, DMA_WRITE_V_SIZE1,
				cfg->planes[cfg->mode_420 ? 0 : 1].height);
			isp_dma_writel(isp_chan->regs, DMA_WRITE_PIC_SIZE1_REG,
				       reg);
			reg = 0;
			DCT_REGS_FIELD_SET(reg, DMA_WRITE_H_SIZE2,
					   cfg->planes[2].width);
			DCT_REGS_FIELD_SET(reg, DMA_WRITE_V_SIZE2,
					   cfg->planes[2].height);
			isp_dma_writel(isp_chan->regs, DMA_WRITE_PIC_SIZE2_REG,
				       reg);
			reg = 0;
			DCT_REGS_FIELD_SET(reg, DMA_WRITE_H_SIZE3,
					   cfg->planes[3].width);
			DCT_REGS_FIELD_SET(reg, DMA_WRITE_V_SIZE3,
					   cfg->planes[3].height);
			isp_dma_writel(isp_chan->regs, DMA_WRITE_PIC_SIZE3_REG,
				       reg);
		}
	}

	/* Always update address registers */
	/* Move out lower 8 bits of address (they are not programmed to the register) */
	/* Skip dummy planes, plane addresses must be programmed in the active planes. */
	for (int i = 0; i < ISP_DMA_NUM_PLANES; i++) {
		if (!cfg->planes[i].dummy) {
			if (isp_chan->direct_mode) {
				addr[i] = isp_chan->direct_mode_addr
						  [source_index++];
			} else if (desc) {
				addr[i] = desc->plane_addresses[source_index++];
			} else {
				// @todo leaky was introduced for testing Remove when investigations are complete.
				if (isp_chan->leaky_mem_avail &&
				    isp_chan->leaky_mode_addr_dma[source_index]) {
					// program a dummy dma to avoid back pressure
					addr[i] = isp_chan->leaky_mode_addr_dma
							  [source_index++];
					isp_chan->dma_stats.overrun++;
				} else {
					// unused and invalid
					addr[i] = 0xdeadbeef;
				}
			}
		} else {
			// unused and invalid
			addr[i] = 0xdeadbeef;
		}
	}
	reg = 0;
	DCT_REGS_FIELD_SET(reg, DMA_READ_BA0, addr[0] >> 8);
	isp_dma_writel(isp_chan->regs, DMA_READ_BA0_REG, reg);
	reg = 0;
	DCT_REGS_FIELD_SET(reg, DMA_READ_BA1, addr[1] >> 8);
	isp_dma_writel(isp_chan->regs, DMA_READ_BA1_REG, reg);
	reg = 0;
	DCT_REGS_FIELD_SET(reg, DMA_READ_BA2, addr[2] >> 8);
	isp_dma_writel(isp_chan->regs, DMA_READ_BA2_REG, reg);
	reg = 0;
	DCT_REGS_FIELD_SET(reg, DMA_READ_BA3, addr[3] >> 8);
	isp_dma_writel(isp_chan->regs, DMA_READ_BA3_REG, reg);
	//printk("BA0: 0x%llx (%s)\n",  addr[0], isp_chan->common.name);
}

/* -----------------------------------------------------------------------------
 * Tasklets and Timers
 */

/**
 * isp_dma_cleanup_lists - cleanup lists and descriptors
 *
 * @chan: ISP DMA channel with finished transactions.
 */
static void isp_dma_cleanup_lists(struct isp_dma_channel *isp_chan)
{
	struct isp_dma_tx_descriptor *desc, *next;

	// Move unprocessed descriptors to done list and cleanup
	if (isp_chan->desc_active) {
		if (isp_chan->desc_active != &desc_leaky) {
			list_add_tail(&isp_chan->desc_active->node,
				      &isp_chan->desc_issued_list);
		}
		isp_chan->desc_active = NULL;
	}

	if (isp_chan->desc_next) {
		if (isp_chan->desc_next != &desc_leaky) {
			list_add_tail(&isp_chan->desc_next->node,
				      &isp_chan->desc_issued_list);
		}
		isp_chan->desc_next = NULL;
	}

	list_for_each_entry_safe(desc, next, &isp_chan->desc_pending_list,
				 node) {
		list_del(&desc->node);
		list_add_tail(&desc->node, &isp_chan->desc_done_list);
	}

	list_for_each_entry_safe(desc, next, &isp_chan->desc_issued_list,
				 node) {
		list_del(&desc->node);
		list_add_tail(&desc->node, &isp_chan->desc_done_list);
	}

	list_for_each_entry_safe(desc, next, &isp_chan->desc_done_list, node) {
		list_del(&desc->node);
		kfree(desc);
	}
}

/**
 * isp_dma_desc_finish - Notify client that transactions are complete and do cleanup
 *
 * @chan: ISP DMA channel with finished transactions.
 */
static void isp_dma_desc_finish(struct isp_dma_channel *isp_chan)
{
	struct isp_dma_tx_descriptor *desc, *next;
	unsigned long flags;
	struct dmaengine_result result;

	/* TODO: Is this lock needed? In interrupt context new entries may be
	 * added to the end of the list, but that should not be an issue. */
	spin_lock_irqsave(&isp_chan->desc_lock, flags);

	/* Loop over all descriptors in the done list */
	list_for_each_entry_safe(desc, next, &isp_chan->desc_done_list, node) {
		list_del(&desc->node);
		result.result = DMA_TRANS_NOERROR;
		result.residue = 0;
		dmaengine_desc_get_callback_invoke(&desc->common, &result);
		// Free the descriptor
		kfree(desc);
	}

	spin_unlock_irqrestore(&isp_chan->desc_lock, flags);
}

/**
 * isp_dma_run_tasklet_cleanup - Task that runs after transaction completion
 *
 * @task: Tasklet structure which contains the DMA channel as data.
 *
 * This task is scheduled when a transaction is completed. It calls
 * isp_dma_desc_finish() to notify the client and perform cleanup.
 */
static void isp_dma_run_tasklet_cleanup(struct tasklet_struct *task)
{
	struct isp_dma_channel *isp_chan =
		from_tasklet(isp_chan, task, tasklet_cleanup);
	isp_dma_desc_finish(isp_chan);
}

/**
 * isp_dma_run_timer_restart - Timer that runs after updating the configuration
 *
 * @data: Data pointer passed to the structure which contains the DMA channel as data.
 *
 * This timer is started when a transaction is completed and an automatic
 * config update is pending. If the automatic update is not being performed
 * due to a race condition this ensures that the DMA does not get stuck.
 */
static void isp_dma_run_timer_restart(struct timer_list *timer)
{
	struct isp_dma_channel *isp_chan =
		from_timer(isp_chan, timer, timer_restart);
	struct dma_chan *chan = &isp_chan->common;
	struct device *dev = chan->device->dev;

	/* Restart timer has elapsed, that means config update did not happen
	 * and a new transfer has to be started manually. */
	dev_dbg(dev,
		"Channel %s: Restart timer elapsed, performing manual DMA restart\n",
		chan->name);
	isp_dma_config_update(isp_chan, true);
}

/* -----------------------------------------------------------------------------
 * Required DMA device functions
 */

/**
 * isp_dma_alloc_chan_resources - Allocate channel resources
 * @chan: DMA channel which is contained by a isp_dma_channel structure.
 *
 * Return: 0 on success, negative error code otherwise
 */
static int isp_dma_alloc_chan_resources(struct dma_chan *chan)
{
	struct device *dev = chan->device->dev;

	/* Initialize cookie */
	dma_cookie_init(chan);
	dev_dbg(dev, "%s", __FUNCTION__);

	return 0;
}

/**
 * isp_dma_free_chan_resources - Free channel resources
 * @chan: DMA channel which is contained by a isp_dma_channel structure.
 */
static void isp_dma_free_chan_resources(struct dma_chan *chan)
{
	struct device *dev = chan->device->dev;
	dev_dbg(dev, "%s", __FUNCTION__);
}

/**
 * isp_dma_terminate_all - Abort all pending and ongoing transfers
 * @chan: DMA channel which is contained by a isp_dma_channel structure.
 *
 * Return: Always 0
 */
static int isp_dma_terminate_all(struct dma_chan *chan)
{
	struct isp_dma_channel *isp_chan = to_isp_dma_channel(chan);
	struct device *dev = chan->device->dev;
	unsigned long flags;
	//u32 reg;

	dev_err(dev, "Channel %s: Terminating all transfers\n", chan->name);

	spin_lock_irqsave(&isp_chan->desc_lock, flags);

	isp_chan->terminating = true;

	isp_dma_cleanup_lists(isp_chan);

	spin_unlock_irqrestore(&isp_chan->desc_lock, flags);

	return 0;
}

/**
 * isp_dma_synchronize - Wait for all transfers to complete
 * @chan: DMA channel which is contained by a isp_dma_channel structure.
 */
static void isp_dma_synchronize(struct dma_chan *chan)
{
	struct isp_dma_channel *isp_chan = to_isp_dma_channel(chan);
	struct device *dev = chan->device->dev;
	int timeout = 500;
	bool once = true;

	/* Stop the restart timer */
	del_timer_sync(&isp_chan->timer_restart);

	/* Stop the cleanup task, make sure all done descriptors have been
	 * reported to the client and cleaned up. */
	tasklet_kill(&isp_chan->tasklet_cleanup);
	isp_dma_desc_finish(isp_chan);

	/* Wait until terminating flag is no longer set which indicates that
	 * the final config done IRQ has happened and the DMA has been reset. */
	while (isp_chan->terminating && --timeout > 0) {
		if (once) {
			dev_dbg(dev,
				"Channel %s: Waiting for channel to terminate\n",
				chan->name);
			once = false;
		}
		usleep_range(10, 50);
	}
	if (!once)
		dev_dbg(dev, "Channel %s: Done waiting%s\n", chan->name,
			timeout <= 0 ? " (timeout)" : "");

	isp_chan->terminating = false;
}

/**
 * isp_dma_tx_status - Get DMA transaction status
 * @chan: DMA channel which is contained by a isp_dma_channel structure.
 * @cookie: Transaction identifier.
 * @txstate: Transaction state.
 *
 * Return: DMA transaction status
 */
static enum dma_status isp_dma_tx_status(struct dma_chan *chan,
					 dma_cookie_t cookie,
					 struct dma_tx_state *txstate)
{
	enum dma_status ret;

	ret = dma_cookie_status(chan, cookie, txstate);

	return ret;
}

/**
 * isp_dma_issue_pending - Issue pending transaction
 * @chan: DMA channel which is contained by a isp_dma_channel structure.
 */
static void isp_dma_issue_pending(struct dma_chan *chan)
{
	struct isp_dma_channel *isp_chan = to_isp_dma_channel(chan);
	struct isp_dma_tx_descriptor *desc, *next;
	unsigned long flags;

	/* Move descriptors from pending list to issued list */
	spin_lock_irqsave(&isp_chan->desc_lock, flags);
	list_for_each_entry_safe(desc, next, &isp_chan->desc_pending_list,
				 node) {
		list_del(&desc->node);
		list_add_tail(&desc->node, &isp_chan->desc_issued_list);
	}
	desc = NULL;

	/* Check if there is already a transfer prepared, if so it will be
	 * started automatically with the next config update interrupt. */
	if (isp_chan->desc_next) {
		goto done;
	}

	/* Else try to get a new descriptor from the issued list */
	desc = list_first_entry_or_null(&isp_chan->desc_issued_list,
					struct isp_dma_tx_descriptor, node);
	if (unlikely(!desc)) {
		goto done;
	}

	/* If a valid descriptor was fetched, remove it from the issued
	 * list and program its settings to the shadow registers. */
	isp_chan->terminating = false;
	list_del(&desc->node);
	isp_dma_program_transfer(isp_chan, desc);

	/* If there is still an active transfer ongoing, do not interrupt
	 * it, but schedule an automatic shadow update at transfer end if
	 * we are using the double mapped approach. */
	if (isp_chan->desc_active) {
		isp_chan->desc_next = desc;
		isp_dma_config_update(isp_chan, false);
	}
	/* Else immediately apply the new configuration to start the DMA. */
	else {
		isp_chan->desc_active = desc;
		if (!isp_chan->direct_mode) {
			isp_dma_config_update(isp_chan, true);
		}
	}

done:
	spin_unlock_irqrestore(&isp_chan->desc_lock, flags);
}

// @todo leaky was introduced for testing Remove when investigations are complete.
void isp_dma_leaky_buffer_free(struct isp_dma_channel *isp_chan)
{
	struct dma_chan *chan = &isp_chan->common;
	struct device *dev = chan->device->dev;

	if (isp_chan->leaky_mem_avail) {
		/* Free the buffers used for leaky output */
		for (int i = 0; i < ISP_DMA_NUM_PLANES; i++) {
			if (isp_chan->leaky_mode_addr_cpu[i]) {
				uint buf_size = isp_chan->cfg.planes[i].stride *
						isp_chan->cfg.planes[i].height;

				dma_free_coherent(
					dev, buf_size,
					isp_chan->leaky_mode_addr_cpu[i],
					isp_chan->leaky_mode_addr_dma[i]);

				isp_chan->leaky_mode_addr_cpu[i] = NULL;
			}
		}
	}
	isp_chan->leaky_mem_avail = false;
}

/**
 * isp_dma_device_config - Configure the DMA channel
 * @chan: DMA channel which is contained by a isp_dma_channel structure.
 * @config: Channel configuration.
 *
 * Return: 0 on success, negative error code otherwise
 *
 * The ISP DMA only uses the peripheral config pointer to pass a custom
 * configuration. All other members of the dma_slave_config structure are
 * ignored and can be set to any value.
 *
 * We could check the transfer direction to match the channel but that
 * member is deprecated anyway and should not be used anymore.
 *
 * We could also check the address width and the burst size parameters,
 * but those are fixed in HW for this DMA:
 * To/from memory: 256 bit AXI interface, burst length 8
 *                 -> burst size: 256 byte
 * To/from device: 48 bit streaming interface
 *
 * The peripheral_config member of the slave config struct must point
 * to an isp_dma_channel_config structure, for details see the
 * documentation of that struct.
 */
static int isp_dma_device_config(struct dma_chan *chan,
				 struct dma_slave_config *config)
{
	struct isp_dma_channel *isp_chan = to_isp_dma_channel(chan);
	struct dma_device *dma_dev = chan->device;
	struct isp_dma_channel_config *cfg;
	struct isp_dma_plane_config *plane;
	struct device *dev = dma_dev->dev;
	int i, byte_per_pixel, width_bytes;
	u8 comp_size, comp_size_sum = 0;

	/* Check peripheral config size */
	if (config->peripheral_size != sizeof(*cfg)) {
		dev_err(dev,
			"Channel %s: Peripheral config has unexpected size (got %ld, need %ld)\n",
			chan->name, config->peripheral_size, sizeof(*cfg));
		return -ENOMEM;
	}
	cfg = config->peripheral_config;
	cfg->active_planes = 0;

	/* Check configuration */
	for (i = 0; i < ISP_DMA_NUM_PLANES; i++) {
		comp_size = cfg->planes[i].comp_size;
		// Check component size does not exceed 48 bit
		if (comp_size > 48) {
			dev_err(dev,
				"Channel %s: Pixel size for component %d exceeds 48 bit (is %d)\n",
				chan->name, i, comp_size);
			return -EINVAL;
		}
		// For active planes check parameters
		plane = &cfg->planes[i];
		if (comp_size > 0) {
			// Count number of active planes and check for illegal gaps
			cfg->active_planes++;
			if (i >= cfg->active_planes) {
				dev_err(dev,
					"Channel %s: Gap in component size definition between plane %d and %d\n",
					chan->name, cfg->active_planes - 1, i);
				return -EINVAL;
			}
			// Check width and height are within valid range
			if (plane->width < ISP_DMA_MIN_WIDTH ||
			    plane->width > ISP_DMA_MAX_WIDTH) {
				dev_err(dev,
					"Channel %s: Plane %d: Width %d not within valid range [%d, %d]\n",
					chan->name, i, plane->width,
					ISP_DMA_MIN_WIDTH, ISP_DMA_MAX_WIDTH);
				return -EINVAL;
			}
			if (plane->height < ISP_DMA_MIN_HEIGHT ||
			    plane->height > ISP_DMA_MAX_HEIGHT) {
				dev_err(dev,
					"Channel %s: Plane %d: Height %d not within valid range [%d, %d]\n",
					chan->name, i, plane->height,
					ISP_DMA_MIN_HEIGHT, ISP_DMA_MAX_HEIGHT);
				return -EINVAL;
			}
			/* For read channel: Check that width and height are identical for all
			 * planes (HW limitation), except for 4:2:0 mode which has other limitations
			 * which are checked below. */
			if (IS_RD_CHAN(isp_chan) && !cfg->mode_420 && i > 0 &&
			    (plane->width != cfg->planes[0].width ||
			     plane->height != cfg->planes[0].height)) {
				dev_err(dev,
					"Channel %s: For read channels width and height must be identical for all planes (mismatch in plane %d)\n",
					chan->name, i);
				return -EINVAL;
			}
			// Check stride if specified
			byte_per_pixel = (comp_size + 7) >>
					 3; // Round up to nearest byte boundary
			if (plane->stride > 0) {
				// Check max stride
				if (plane->stride > ISP_DMA_MAX_STRIDE) {
					dev_err(dev,
						"Channel %s: Plane %d: Stride %d exceeds maximum %d\n",
						chan->name, i, plane->stride,
						ISP_DMA_MAX_STRIDE);
					return -EINVAL;
				}
				// Check alignment
				if (plane->stride % 256 != 0) {
					dev_err(dev,
						"Channel %s: Plane %d: Stride %d is not aligned to 256 byte\n",
						chan->name, i, plane->stride);
					return -EINVAL;
				}
				// Check size
				width_bytes = plane->width * byte_per_pixel;
				if (width_bytes > plane->stride) {
					dev_err(dev,
						"Channel %s: Plane %d: Width in bytes (%d) is bigger than stride (%d)\n",
						chan->name, i, width_bytes,
						plane->stride);
					return -EINVAL;
				}
			}
			// Calculate minimum stride if not specified
			else {
				plane->stride =
					(byte_per_pixel * plane->width + 255) &
					~255;
				// Check if stride is within valid range
				if (plane->stride > ISP_DMA_MAX_STRIDE) {
					dev_err(dev,
						"Channel %s: Plane %d: Auto-calculated stride %d exceeds maximum %d, width to big for selected pixel format\n",
						chan->name, i, plane->stride,
						ISP_DMA_MAX_STRIDE);
					return -EINVAL;
				}
			}
		}
		// For inactive planes log if plane parameters are not 0
		else if (plane->stride != 0 || plane->width != 0 ||
			 plane->height != 0) {
			dev_info(
				dev,
				"Channel %s: Component %d is not active (0), but stride (%d), width (%d) or height (%d) are specified (>0)\n",
				chan->name, i, plane->stride, plane->width,
				plane->height);
		}
		// Sum up component sizes for single-plane check (see below)
		comp_size_sum += comp_size;
	}
	// There must be at least 1 active plane
	if (cfg->active_planes == 0) {
		dev_err(dev,
			"Channel %s: There must be at least one active plane with component size > 0\n",
			chan->name);
		return -EINVAL;
	}
	// Sum of component sizes must not exceed 48 bit in single-plane mode
	if (!cfg->multi_lane && comp_size_sum > 48) {
		dev_err(dev,
			"Channel %s: Single-plane mode: Sum of component sizes exceeds 48 bit (is %d)\n",
			chan->name, comp_size_sum);
		return -EINVAL;
	}
	// Check limitations of 4:2:0 mode
	if (cfg->mode_420) {
		// The number of planes must be 2
		if (cfg->active_planes != 2) {
			dev_err(dev,
				"Channel %s: In 4:2:0 mode the number of active planes must be 2 (is %d)\n",
				chan->name, cfg->active_planes);
			return -EINVAL;
		}
		/* The second plane (e.g. U/V or Cb/Cr) must have halve the size of the first
		 * plane (e.g. Y). Internally the DMA uses the full plane size for all planes but
		 * skips even lines for the second plane if 4:2:0 mode is enabled. */
		if (cfg->planes[0].width != cfg->planes[1].width << 1 ||
		    cfg->planes[0].height != cfg->planes[1].height << 1) {
			dev_err(dev,
				"Channel %s: In 4:2:0 mode the second plane (%dx%d) must have half the width and height of the first plane (%dx%d))\n",
				chan->name, cfg->planes[1].width,
				cfg->planes[1].height, cfg->planes[0].width,
				cfg->planes[0].height);
			return -EINVAL;
		}
	}

	/* Copy config to channel config */
	spin_lock(&isp_chan->cfg_lock);
	memcpy(&isp_chan->cfg, cfg, sizeof(isp_chan->cfg));
	// Increment config ID, in case of overflow: Skip ID 0 which indicates an invalid config
	isp_chan->cfg_id++;
	if (isp_chan->cfg_id == 0) {
		isp_chan->cfg_id = 1;
	}
	dev_dbg(dev, "Channel %s: Update config, new ID: %d\n", chan->name,
		isp_chan->cfg_id);
	spin_unlock(&isp_chan->cfg_lock);

	/* Print config info */
	dev_info(
		dev,
		"Channel %s: Got config ID %d, has %d active planes in %s%s-lane mode:\n",
		chan->name, isp_chan->cfg_id, cfg->active_planes,
		cfg->mode_420 ? "4:2:0-" : "",
		cfg->multi_lane ? "multi" : "single");
	for (i = 0; i < cfg->active_planes; i++) {
		dev_info(
			dev,
			"Channel %s: Plane %d: %d bit component, stride %d byte, %dx%d\n",
			chan->name, i, cfg->planes[i].comp_size,
			cfg->planes[i].stride, cfg->planes[i].width,
			cfg->planes[i].height);
	}

	/* We need to setup read channels before any transfer is initiated
	 * by the system, otherwise it will never happen because write buffers
	 * can not finish without an already configured read channel in direct
	 * mode. */
	if (isp_chan->direct_mode) {
		isp_dma_program_transfer(isp_chan, NULL);
		isp_dma_config_update(isp_chan, true);
	} else {
		// @todo leaky was introduced for testing Remove when investigations are complete.
		/* Setup a dummy descriptor to implement a leaky bucket at the output
		 * otherwise a backpressure at the output would stall the complete
		 * pipeline which fries our isp pre module. */
		if (isp_chan->leaky_mem_avail) {
			isp_dma_leaky_buffer_free(isp_chan);
		}
		if (!isp_chan->leaky_mem_avail && IS_WR_CHAN(isp_chan)) {
			for (int i = 0; i < ISP_DMA_NUM_PLANES; i++) {
				uint buf_size =
					(isp_chan->cfg.planes[i].stride *
					 isp_chan->cfg.planes[i].height);

				if (buf_size &&
				    !isp_chan->cfg.planes[i].dummy) {
					dev_info(
						dev,
						"Channel %s: allocate leaky dma buffer:%d",
						chan->name, buf_size);
					isp_chan->leaky_mode_addr_cpu
						[i] = dma_alloc_coherent(
						dma_dev->dev, buf_size,
						&isp_chan->leaky_mode_addr_dma[i],
						GFP_KERNEL);
				}
			}
			isp_chan->leaky_mem_avail = true;
		}
	}

	return 0;
}

/**
 * isp_dma_tx_submit - Submit DMA transaction
 * @desc: Async transaction descriptor contained by a
 *        isp_dma_tx_descriptor structure.
 *
 * Return: cookie value on success, negative error code otherwise
 */
static dma_cookie_t isp_dma_tx_submit(struct dma_async_tx_descriptor *desc)
{
	struct isp_dma_tx_descriptor *isp_desc = to_isp_dma_tx_descriptor(desc);
	struct isp_dma_channel *isp_chan = to_isp_dma_channel(desc->chan);
	dma_cookie_t cookie;
	unsigned long flags;

	spin_lock_irqsave(&isp_chan->desc_lock, flags);

	cookie = dma_cookie_assign(desc);

	/* Add descriptor to pending queue */
	list_add_tail(&isp_desc->node, &isp_chan->desc_pending_list);
	spin_unlock_irqrestore(&isp_chan->desc_lock, flags);

	return cookie;
}

/**
 * isp_dma_prep_slave_sg - Prepare descriptor for a DMA_SLAVE transaction
 * @chan: DMA channel which is contained by a isp_dma_channel structure.
 * @sgl: Scatterlist to transfer to/from.
 * @sg_len: Number of entries in @scatterlist.
 * @direction: DMA direction.
 * @flags: Transfer acknowledge flags.
 * @context: DMA transfer context (unused, can be NULL).
 *
 * Return: Async transaction descriptor on success and NULL on failure
 *
 * The ISP DMA does not support real scatter/gather operations, it rather
 * uses the scatter list as means to setup the base addresses for all active
 * planes of the DMA channel (up to ISP_DMA_NUM_PLANES). That means there
 * must be one scatterlist entry for each active plane, even if the memory
 * is contiguous and the planes lie in adjacent buffers in memory.
 *
 * The scatterlist entries must follow these rules:
 * - The DMA addresses for each entry must point to contiguous, DMA-able
 *   memory, but there can be gaps between the planes. The addresses
 *   must be 256 byte aligned (which they should be anyway since the
 *   kernel usually allocates DMA buffers page aligned).
 * - The offsets of the scatterlist entries must be 0.
 * - The length describes the physical size of the contiguous memory
 *   segment.
 *
 * The driver checks the address alignment and if the buffers have
 * sufficient size for the current channel configuration that has been
 * set using isp_dma_device_config().
 */
static struct dma_async_tx_descriptor *
isp_dma_prep_slave_sg(struct dma_chan *chan, struct scatterlist *sgl,
		      unsigned int sg_len,
		      enum dma_transfer_direction direction,
		      unsigned long flags, void *context)
{
	struct isp_dma_channel *isp_chan = to_isp_dma_channel(chan);
	struct dma_device *dma_dev = chan->device;
	struct device *dev = dma_dev->dev;
	struct isp_dma_tx_descriptor *desc;
	struct scatterlist *sg;
	int i, active_planes = 0;
	unsigned int min_size;

	/* Check transfer direction matches this channel (channel direction is
	 * fixed in HW and can not be changed). */
	if (direction != isp_chan->direction) {
		dev_err(dev,
			"Channel %s: DMA channel does not have the requested direction (want %d, has %d)\n",
			chan->name, direction, isp_chan->direction);
		return NULL;
	}

	/* Check if there is a valid channel config (isp_dma_device_config()
	 * has been called at least once). */
	spin_lock(&isp_chan->cfg_lock);
	if (isp_chan->cfg_id == 0) {
		dev_err(dev,
			"Channel %s: DMA channel has not been configured yet\n",
			chan->name);
		return NULL;
	}
	spin_unlock(&isp_chan->cfg_lock);

	/* Allocate descriptor */
	desc = kzalloc(sizeof(*desc), GFP_NOWAIT);
	if (!desc) {
		dev_err(dev,
			"Channel %s: Failed to allocate memory for TX descriptor\n",
			chan->name);
		return NULL;
	}

	/* Initialize descriptor */
	dma_async_tx_descriptor_init(&desc->common, chan);
	desc->common.flags = flags;
	desc->common.tx_submit = isp_dma_tx_submit;
	// Copy config from global channel config
	spin_lock(&isp_chan->cfg_lock);
	memcpy(&desc->cfg, &isp_chan->cfg, sizeof(desc->cfg));
	desc->cfg_id = isp_chan->cfg_id;
	spin_unlock(&isp_chan->cfg_lock);

	/* Get number of active planes */
	for (i = 0; i < ISP_DMA_NUM_PLANES; i++) {
		if (desc->cfg.planes[i].comp_size == 0) {
			break;
		}
		if (!desc->cfg.planes[i].dummy) {
			active_planes++;
		}
	}

	/* SG list must have one entry for each plane */
	if (sg_len != active_planes) {
		dev_err(dev,
			"Channel %s: Invalid amount of planes in scatterlist: %d != %d, valid range [0, %d]\n",
			chan->name, sg_len, active_planes, ISP_DMA_NUM_PLANES);
		goto error;
	}

	/* Get DMA addresses from SG list and do error checks */
	for_each_sg(sgl, sg, sg_len, i) {
		// Check DMA address is 256 byte aligned
		if (sg->dma_address % 256 != 0) {
			dev_err(dev,
				"Channel %s: DMA buffer address for plane %d is not 256 byte aligned: %p\n",
				chan->name, i, (void *)sg->dma_address);
			goto error;
		}
		// Check buffer size is sufficient
		min_size =
			desc->cfg.planes[i].stride * desc->cfg.planes[i].height;
		if (sg->length < min_size) {
			dev_err(dev,
				"Channel %s: DMA buffer for plane %d is to small: Got %d, need %d bytes\n",
				chan->name, i, sg->length, min_size);
			goto error;
		}
		// Store address in descriptor
		desc->plane_addresses[i] = sg->dma_address;
	}

	return &desc->common;

error:
	kfree(desc);

	return NULL;
}

/* -----------------------------------------------------------------------------
 * Interrupt handling
 */

/**
 * isp_dma_irq_handler_transfer_done - "Transfer Done" interrupt handler
 * @irq: IRQ number.
 * @data: Pointer to the DMA channel structure.
 *
 * Return: Always IRQ_HANDLED
 *
 * This interrupt indicates that the active transfer has been finished and
 * all data has been written to or read from memory.
 *
 * In this interrupt move the descriptor of the finished transfer from "active"
 * to the "done" list, schedule the descriptor cleanup task and start the
 * restart timer in case of a possible race condition.
 */
static irqreturn_t isp_dma_irq_handler_transfer_done(int irq, void *data)
{
	struct isp_dma_channel *isp_chan = data;
	struct dma_chan *chan = &isp_chan->common;

	spin_lock(&isp_chan->desc_lock);
	isp_chan->dma_stats.transfer_done++;

	/* Check if processing shall be stopped, if so, do nothing */
	if (isp_chan->terminating) {
		isp_dma_disable_transfer(chan);
		isp_dma_soft_reset(isp_chan);
		isp_chan->terminating = false;
		spin_unlock(&isp_chan->desc_lock);
		return IRQ_HANDLED;
	}

	/* Move active descriptor to done list */
	if (likely(isp_chan->desc_active)) {
		if (isp_chan->desc_active != &desc_leaky) {
			dma_cookie_complete(&isp_chan->desc_active->common);
			list_add_tail(&isp_chan->desc_active->node,
				      &isp_chan->desc_done_list);
		}
		isp_chan->desc_active = NULL;
	} else {
		isp_chan->dma_stats.no_active_desc++;
	}

	/* If there is a new config pending (already programmed to the shadow
	 * registers), start the restart timer to ensure the update is actually
	 * performed and was not missed due to a race condition.
	 * This is not valid for direct-mode. */
	if (isp_chan->desc_next && !isp_chan->direct_mode)
		mod_timer(&isp_chan->timer_restart,
			  jiffies + msecs_to_jiffies(1));

	spin_unlock(&isp_chan->desc_lock);

	/* Schedule cleanup task */
	tasklet_schedule(&isp_chan->tasklet_cleanup);

	return IRQ_HANDLED;
}

/**
 * isp_dma_irq_handler_config_done - "Config Done" interrupt handler
 * @irq: IRQ number.
 * @data: Pointer to the DMA channel structure.
 *
 * Return: Always IRQ_HANDLED
 *
 * This interrupt indicates that the configuration of the DMA has been
 * successfully updated, that means the shadow registers have been copied
 * to the active registers and processing of the next transfer has started.
 *
 * In this interrupt the current "next" descriptor is moved to the "active"
 * descriptor. Then a new "next" descriptor from the "issued" list ist fetched
 * (if it is not empty), its settings are programmed to the shadow registers
 * and an automatic config update is scheduled.
 *
 * If no new descriptor is available the now "active" descriptor is the
 * last descriptor and automatic config update is not enabled. A new transfer
 * has to be started manually by the client by issuing new transfers.
 */
static irqreturn_t isp_dma_irq_handler_config_done(int irq, void *data)
{
	struct isp_dma_channel *isp_chan = data;
	struct dma_chan *chan = &isp_chan->common;
	struct device *dev = chan->device->dev;
	struct isp_dma_tx_descriptor *desc;

	isp_chan->dma_stats.config_done++;

	/* Check if processing shall be stopped */
	if (isp_chan->terminating) {
		/* Set terminating to false, this is the signal for
		 * isp_dma_synchronize() that the DMA has been stopped. */
		isp_chan->terminating = false;
		return IRQ_HANDLED;
	}

	if (isp_chan->direct_mode) {
		isp_dma_config_update(isp_chan, false);
		return IRQ_HANDLED;
	}

	spin_lock(&isp_chan->desc_lock);

	/* Update was performed, stop the restart timer */
	del_timer(&isp_chan->timer_restart);

	/* Move next descriptor to active descriptor */
	/* For the config update interrupt there will be no next descriptor as
	 * the interrupt gets triggered immediately after force starting the
	 * first transfer. */
	if (isp_chan->desc_next) {
		/* It is expected that the active transfer has already been cleaned up
		 * by the transfer done IRQ. */
		if (unlikely(isp_chan->desc_active)) {
			if (isp_chan->desc_active != &desc_leaky) {
				dev_err(dev,
					"Channel %s: Got cfg done IRQ but active descriptor is still set, assuming transfer is done\n",
					chan->name);
				list_add_tail(&isp_chan->desc_active->node,
					      &isp_chan->desc_done_list);
			}
			isp_chan->desc_active = NULL;
		}
		// Move next descriptor to active descriptor
		isp_chan->desc_active = isp_chan->desc_next;
		isp_chan->desc_next = NULL;
	}
	/* In any case the next descriptor is now NULL and we can try to
	 * get a new descriptor from the issued list. */
	desc = list_first_entry_or_null(&isp_chan->desc_issued_list,
					struct isp_dma_tx_descriptor, node);
	if (desc) {
		list_del(&desc->node);
		isp_chan->desc_next = desc;
	} else if (IS_WR_CHAN(isp_chan)) {
		isp_chan->desc_next = &desc_leaky;
	}
	spin_unlock(&isp_chan->desc_lock);

	/* Program next transfer and schedule automatic config update,
	 * For write channels, if desc is NULL, a leaky buffer address
	 * will be used to avoid FIFO overflows and back pressure
	 * everywhere (HW work-around). */
	if (desc || IS_WR_CHAN(isp_chan)) {
		isp_dma_program_transfer(isp_chan, desc);
		isp_dma_config_update(isp_chan, false);
	}

	return IRQ_HANDLED;
}

/* -----------------------------------------------------------------------------
 * DMA channel probe / remove
 */

/**
 * isp_dma_channel_remove - Remove DMA channel from controller
 * @chan: ISP DMA channel that shall be removed.
 */
static void isp_dma_channel_remove(struct isp_dma_channel *isp_chan)
{
	struct dma_chan *chan = &isp_chan->common;
	struct device *dev = chan->device->dev;

	dev_dbg(dev, "%s", __FUNCTION__);

	// @todo leaky was introduced for testing Remove when investigations are complete.
	isp_dma_leaky_buffer_free(isp_chan);

	/* Unmap IO memory */
	if (isp_chan->regs)
		iounmap(isp_chan->regs);

	/* Free interrupts */
	if (isp_chan->irq_transfer_done >= 0)
		free_irq(isp_chan->irq_transfer_done, isp_chan);

	if (isp_chan->irq_config_done >= 0)
		free_irq(isp_chan->irq_config_done, isp_chan);

	/* Stop tasks and timers */
	tasklet_kill(&isp_chan->tasklet_cleanup);
	del_timer(&isp_chan->timer_restart);

	/* Remove channel from channel list */
	if (!list_empty(&isp_chan->common.device_node))
		list_del(&isp_chan->common.device_node);
}

static int isp_dma_channel_parse_interconnect(struct device *dev,
					      struct isp_dma_channel *isp_chan,
					      struct device_node *node)
{
	struct device_node *interconnect_node;
	struct device_node *direct_conn_node;
	bool is_src_chan = (isp_chan->direction == DMA_DEV_TO_MEM);
	u32 direct_addr[ISP_DMA_NUM_PLANES];
	int ret = 0;

	/* Check if interconnect property is present */
	interconnect_node = of_parse_phandle(node, "interconnect", 0);
	if (!interconnect_node) {
		goto bail;
	}

	/* Parse interconnect property based on this channel's direction */
	direct_conn_node = of_parse_phandle(
		interconnect_node,
		is_src_chan ? "direct-dest" : "direct-source", 0);
	if (!direct_conn_node) {
		dev_err(dev, "Missing interconnect node properties\n");
		ret = -EINVAL;
		goto bail;
	}
	if (of_property_read_u32_array(direct_conn_node, "direct-mode-addr",
				       direct_addr, ISP_DMA_NUM_PLANES) != 0) {
		dev_err(dev, "Direct mode activated without addr\n");
		ret = -EINVAL;
		goto bail;
	}
	for (int i = 0; i < ISP_DMA_NUM_PLANES; i++) {
		isp_chan->direct_mode_addr[i] = direct_addr[i];
		dev_dbg(dev, "Direct mode set with addr 0x%llx",
			isp_chan->direct_mode_addr[i]);
	}
	isp_chan->direct_mode = true;

	if (!is_src_chan) {
		/* get access to isp_dma driver which should be used as
		 * a direct source via the device tree node */
		dev_info(dev, "Direct mode with direct-source: %s %s\n",
			 direct_conn_node->name, direct_conn_node->full_name);

		isp_chan->direct_source =
			of_find_device_by_node(direct_conn_node);
		of_node_put(direct_conn_node);
		if (!isp_chan->direct_source) {
			ret = -EPROBE_DEFER;
			goto bail;
		}
	}

bail:
	return ret;
}

/**
 * isp_dma_channel_probe - Initialize DMA channel from device tree
 * @dma_dev: ISP DMA device structure which contains the DMA
 * 	     channel structure that shall be initialized.
 * @node: Device tree node for this channel (child of dma_dev)
 *
 * Return: 0 on success, -EPROBE_DEFER if IRQs are not available
 *         yet, negative error codes otherwise
 */
static int isp_dma_channel_probe(struct dma_device *dma_dev,
				 struct device_node *node)
{
	struct isp_dma_channel *isp_chan;
	struct device *dev = dma_dev->dev;
	u32 video_dev_nr;
	int ret;

	/* Allocate and initialize the channel structure */
	isp_chan = devm_kzalloc(dev, sizeof(*isp_chan), GFP_KERNEL);
	if (!isp_chan)
		return -ENOMEM;

	spin_lock_init(&isp_chan->cfg_lock);
	spin_lock_init(&isp_chan->desc_lock);
	INIT_LIST_HEAD(&isp_chan->desc_pending_list);
	INIT_LIST_HEAD(&isp_chan->desc_issued_list);
	INIT_LIST_HEAD(&isp_chan->desc_done_list);
	INIT_LIST_HEAD(&isp_chan->common.device_node);
	isp_chan->irq_transfer_done = -1;
	isp_chan->irq_config_done = -1;

	if (of_device_is_compatible(node, "dct,isp-dma-wr-channel"))
		isp_chan->direction = DMA_DEV_TO_MEM;
	else if (of_device_is_compatible(node, "dct,isp-dma-rd-channel"))
		isp_chan->direction = DMA_MEM_TO_DEV;
	else {
		dev_err(dev, "Unknown compatible for DMA channel\n");
		return -EINVAL;
	}

	/* Try to get video device number from device tree. If none set, use
	 * -1 (auto) as default. The number can be querried by the user with
	 * isp_dma_get_video_device_number(). */
	if (of_property_read_u32(node, "video-dev-nr", &video_dev_nr) == 0) {
		isp_chan->video_dev_nr = video_dev_nr;
		dev_info(dev, "Using video device number %d from device tree",
			 video_dev_nr);
	} else {
		isp_chan->video_dev_nr = -1;
		dev_info(dev, "Using automatic video device number");
	}

	isp_chan->metadata = of_property_read_bool(node, "metadata");
	if (isp_chan->metadata)
		dev_info(dev, "Metadata channel");

	/* Parse interconnect property to check for direct-mode */
	ret = isp_dma_channel_parse_interconnect(dev, isp_chan, node);
	if (ret) {
		goto error;
	}

	/* Map IO memory */
	isp_chan->regs = of_iomap(node, 0);
	if (!isp_chan->regs) {
		dev_err(dev, "Failed to map registers for DMA channel\n");
		ret = -ENOMEM;
		goto error;
	}

	/* Request interrupts */
	/* Transfer Done IRQ */
	isp_chan->irq_transfer_done = of_irq_get_byname(node, "transfer-done");
	if (isp_chan->irq_transfer_done < 0) {
		ret = dev_err_probe(dev, isp_chan->irq_transfer_done,
				    "Failed to get transfer-done IRQ\n");
		goto error;
	}
	ret = request_irq(isp_chan->irq_transfer_done,
			  isp_dma_irq_handler_transfer_done, 0, node->name,
			  isp_chan);
	if (ret) {
		dev_err(dev, "Unable to request transfer done IRQ %d\n",
			isp_chan->irq_transfer_done);
		isp_chan->irq_transfer_done = -1;
		goto error;
	}
	/* Config Done IRQ */
	isp_chan->irq_config_done = of_irq_get_byname(node, "config-done");
	if (isp_chan->irq_config_done < 0) {
		ret = dev_err_probe(dev, isp_chan->irq_config_done,
				    "Failed to get config-done IRQ\n");
		goto error;
	}
	ret = request_irq(isp_chan->irq_config_done,
			  isp_dma_irq_handler_config_done, 0, node->name,
			  isp_chan);
	if (ret) {
		dev_err(dev, "Unable to request config done IRQ %d\n",
			isp_chan->irq_config_done);
		isp_chan->irq_config_done = -1;
		goto error;
	}

	/* Initialize the tasklets and timers */
	tasklet_setup(&isp_chan->tasklet_cleanup, isp_dma_run_tasklet_cleanup);
	timer_setup(&isp_chan->timer_restart, isp_dma_run_timer_restart, 0);

	/* Register common DMA device in channel and add channel to
	 * channel list. */
	isp_chan->common.device = dma_dev;
	list_add_tail(&isp_chan->common.device_node, &dma_dev->channels);

	// Reset the channel
	isp_dma_soft_reset(isp_chan);

	dev_info(dev, "Probed %s channel: %s, HW ID: %x\n",
		 dmaengine_get_direction_text(isp_chan->direction),
		 node->full_name,
		 isp_dma_readl(isp_chan->regs, DMA_READ_ID_REG));
	return 0;

error:
	isp_dma_channel_remove(isp_chan);
	return ret;
}

/* -----------------------------------------------------------------------------
 * DMA sysfs entries
 */

static ssize_t debug_dma_stats_show(struct device *dev,
				    struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dma_device *dma_dev = platform_get_drvdata(pdev);
	struct dma_chan *chan, *chan_temp;

#define MAX_BUFFER_SIZE 512
	char buffer[MAX_BUFFER_SIZE];
	int offset = 0;

	if (dma_dev && !list_empty(&dma_dev->channels)) {
		list_for_each_entry_safe(chan, chan_temp, &dma_dev->channels,
					 device_node) {
			struct isp_dma_channel *isp_chan =
				to_isp_dma_channel(chan);
			offset += snprintf(&buffer[offset],
					   MAX_BUFFER_SIZE - offset,
					   "%s:\n"
					   "transfer_done:  %u\n"
					   "config_done:    %u\n"
					   "overrun:        %u\n"
					   "no_active_desc: %u\n",
					   chan->name,
					   isp_chan->dma_stats.transfer_done,
					   isp_chan->dma_stats.config_done,
					   isp_chan->dma_stats.overrun,
					   isp_chan->dma_stats.no_active_desc);
		}
	}

	return strlcpy(buf, buffer, PAGE_SIZE);
}

static DEVICE_ATTR_RO(debug_dma_stats);

int isp_dma_create_sysfs(struct platform_device *pdev)
{
	device_create_file(&pdev->dev, &dev_attr_debug_dma_stats);
	return 0;
}

int isp_dma_remove_sysfs(struct platform_device *pdev)
{
	device_remove_file(&pdev->dev, &dev_attr_debug_dma_stats);
	return 0;
}

/* -----------------------------------------------------------------------------
 * DMA platform device
 */

/**
 * isp_dma_probe - Probe ISP DMA device
 * @pdev: Platform device handle.
 *
 * Return: 0 on success, negative error code otherwise
 */
static int isp_dma_probe(struct platform_device *pdev)
{
	struct device_node *node = pdev->dev.of_node;
	struct device_node *child;
	struct dma_device *dma_dev;
	struct dma_chan *chan, *chan_temp;
	struct device *dev = &pdev->dev;
	struct clk *clk;
	int ret;

	dev_info(dev, "Probe ISP DMA\n");

	/* Check if dma-channels property is set. It is ignored by this driver
	 * since channels are configured via child nodes in the device tree, so
	 * give a warning when it is found. */
	if (of_property_read_bool(node, "dma-channels"))
		dev_warn(
			dev,
			"Property 'dma-channels' will be ignored, it is derived from the number of child nodes instead\n");

	/* Allocate DMA device structure */
	dma_dev = devm_kzalloc(dev, sizeof(*dma_dev), GFP_KERNEL);
	if (!dma_dev)
		return -ENOMEM;

	clk = devm_clk_get_enabled(dev, "isp_dma_clk");
	if (IS_ERR(clk)) {
		dev_err(dev, "unable to retrieve isp_dma_clk\n");
	}

	/* The ISP DMA has an address width of 36 bit */
	ret = dma_coerce_mask_and_coherent(dev, DMA_BIT_MASK(36));
	if (ret < 0) {
		dev_err(dev, "DMA mask error %d\n", ret);
		return ret;
	}

	/* Initialize the common DMA engine device */
	dma_dev->dev = dev;
	dma_dev->residue_granularity = DMA_RESIDUE_GRANULARITY_DESCRIPTOR;
	INIT_LIST_HEAD(&dma_dev->channels);
	dma_cap_set(DMA_SLAVE, dma_dev->cap_mask);
	dma_cap_set(DMA_PRIVATE, dma_dev->cap_mask);

	dma_dev->device_alloc_chan_resources = isp_dma_alloc_chan_resources;
	dma_dev->device_free_chan_resources = isp_dma_free_chan_resources;
	dma_dev->device_terminate_all = isp_dma_terminate_all;
	dma_dev->device_synchronize = isp_dma_synchronize;
	dma_dev->device_tx_status = isp_dma_tx_status;
	dma_dev->device_issue_pending = isp_dma_issue_pending;
	dma_dev->device_config = isp_dma_device_config;
	dma_dev->device_prep_slave_sg = isp_dma_prep_slave_sg;

	/* Store ISP device in platform device */
	platform_set_drvdata(pdev, dma_dev);

	/* Initialize the DMA channels */
	INIT_LIST_HEAD(&dma_dev->channels);
	for_each_child_of_node(node, child) {
		ret = isp_dma_channel_probe(dma_dev, child);
		if (ret < 0)
			goto error;
	}

	/* Register the DMA engine */
	ret = dma_async_device_register(dma_dev);
	if (ret) {
		dev_err(dev, "Failed to register DMA device\n");
		goto error;
	}

	ret = of_dma_controller_register(node, of_dma_xlate_by_chan_id,
					 dma_dev);
	if (ret < 0) {
		dev_err(dev, "Unable to register DMA in device tree\n");
		dma_async_device_unregister(dma_dev);
		goto error;
	}

	dev_info(dev, "create sysfs entries\n");
	ret = isp_dma_create_sysfs(pdev);
	if (ret) {
		dev_err(dev, "isp_dma_create_sysfs failed: %d\n", ret);
		goto error;
	}

	dev_info(dev, "DCT ISP DMA probed\n");
	return 0;

error:
	if (!list_empty(&dma_dev->channels)) {
		list_for_each_entry_safe(chan, chan_temp, &dma_dev->channels,
					 device_node)
			isp_dma_channel_remove(to_isp_dma_channel(chan));
	}
	return ret;
}

/**
 * isp_dma_remove - Remove ISP DMA device
 * @pdev: Platform device handle.
 *
 * Return: 0 on success, negative error code otherwise
 */
static int isp_dma_remove(struct platform_device *pdev)
{
	struct dma_device *dma_dev = platform_get_drvdata(pdev);
	struct dma_chan *chan, *chan_temp;

	dev_info(dma_dev->dev, "Remove ISP DMA\n");

	isp_dma_remove_sysfs(pdev);
	of_dma_controller_free(pdev->dev.of_node);
	dma_async_device_unregister(dma_dev);

	if (!list_empty(&dma_dev->channels)) {
		list_for_each_entry_safe(chan, chan_temp, &dma_dev->channels,
					 device_node)
			isp_dma_channel_remove(to_isp_dma_channel(chan));
	}

	platform_set_drvdata(pdev, NULL);

	return 0;
}

static struct of_device_id isp_dma_of_match[] = {
	{
		.compatible = "dct,isp-dma",
	},
	{ /* Sentinel */ },
};
MODULE_DEVICE_TABLE(of, isp_dma_of_match);

static struct platform_driver isp_dma_driver = {
	.probe = isp_dma_probe,
	.remove = isp_dma_remove,
	.driver = {
		.name = "isp-dma",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(isp_dma_of_match),
	},
};

module_platform_driver(isp_dma_driver);

MODULE_DESCRIPTION("DCT ISP-DMA driver");
MODULE_LICENSE("GPL v2");
