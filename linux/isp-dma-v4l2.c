/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 */
//#define DEBUG

#include <linux/lcm.h>
#include <linux/list.h>
#include <linux/module.h>
#include <linux/minmax.h>
#include <linux/of.h>
#include <linux/slab.h>
#include <linux/poison.h>

#include <media/v4l2-dev.h>
#include <media/v4l2-mem2mem.h>
#include <media/v4l2-fh.h>
#include <media/v4l2-ioctl.h>
#include <media/v4l2-event.h>
#include <media/videobuf2-v4l2.h>
#include <media/videobuf2-dma-contig.h>

#include "isp-dma.h"
#include "isp-dma-v4l2.h"

#define video_device_to_isp_dma(vdev) \
	container_of(vdev, struct isp_dma, video_dev)

#define BEEP printk("%s:%d\n", __FUNCTION__, __LINE__);
/* -----------------------------------------------------------------------------
 * Supported video formats
 */

/**
 * struct isp_dma_format - ISP DMA video format description
 * @fourcc: V4L2 pixel format four-character-code identifier.
 * @code: V4L2 media bus format code.
 * @mode_420: If special 420 transfer mode is needed.
 * @num_planes: Number of active planes (must match the amount of components
 *              in comp_sizes that have a size of 0).
 * @comp_sizes: Array of component sizes. A size 0 means plane is not used.
 * @flags: V4L2 format descriptor flags (e.g. V4L2_FMT_FLAG_COMPRESSED).
 */
struct isp_dma_format {
	u32 fourcc;
	u32 code;
	u32 field;
	enum v4l2_colorspace colorspace;
	bool mode_420;
	u8 num_planes;
	u8 comp_sizes[4];
	bool dummy_comp[4];
	u32 flags;
};

/**
 * isp_dma_formats - List of all supported capture formats
 */
static const struct isp_dma_format isp_dma_formats[] = {
	{
		.fourcc = V4L2_PIX_FMT_SRGGB12,
		.code = MEDIA_BUS_FMT_SRGGB12_1X12,
		.field = V4L2_FIELD_NONE,
		.colorspace = V4L2_COLORSPACE_DEFAULT,
		.mode_420 = false,
		.num_planes = 1,
		.comp_sizes = { 12, 0, 0, 0 },
		.dummy_comp = { 0, 0, 0, 0 },
		.flags = V4L2_FMT_FLAG_COMPRESSED,
	},
	{
		.fourcc = V4L2_PIX_FMT_RGB24,
		.code = MEDIA_BUS_FMT_RGB888_3X8,
		.field = V4L2_FIELD_NONE,
		.colorspace = V4L2_COLORSPACE_SRGB,
		.mode_420 = false,
		.num_planes = 1,
		.comp_sizes = { 24, 0, 0, 0 },
		.dummy_comp = { 0, 0, 0, 0 },
		.flags = 0,
	},
	{
		.fourcc = V4L2_PIX_FMT_NV61M,
		.code = MEDIA_BUS_FMT_YUYV8_2X8,
		.field = V4L2_FIELD_NONE,
		.colorspace = V4L2_COLORSPACE_SRGB,
		.mode_420 = false,
		.num_planes = 3,
		.comp_sizes = { 8, 4, 8, 0 },
		.dummy_comp = { 0, 1, 0, 0 },
		.flags = 0,
	}
};

/**
 * isp_dma_num_formats - Number of formats available
 */
static const size_t isp_dma_num_formats = ARRAY_SIZE(isp_dma_formats);

/* -----------------------------------------------------------------------------
 * Helper functions
 */

static struct v4l2_subdev *isp_dma_remote_subdev(struct media_pad *local,
						 u32 *pad)
{
	struct media_pad *remote;

	remote = media_pad_remote_pad_first(local);
	if (!remote || !is_media_entity_v4l2_subdev(remote->entity))
		return NULL;

	if (pad)
		*pad = remote->index;

	return media_entity_to_v4l2_subdev(remote->entity);
}

static int isp_dma_verify_capture_format(struct isp_dma *dma)
{
	struct v4l2_subdev_format fmt;
	struct v4l2_subdev *subdev;
	int ret;

	/* If channel is not a capture channel: Nothing to do */
	if (dma->type != ISP_DMA_TYPE_CAPTURE)
		return 0;

	/* Get connected device */
	subdev = isp_dma_remote_subdev(&dma->pad, &fmt.pad);
	if (!subdev)
		return -EPIPE;
	fmt.which = V4L2_SUBDEV_FORMAT_ACTIVE;
	ret = v4l2_subdev_call(subdev, pad, get_fmt, NULL, &fmt);
	if (ret < 0)
		return ret == -ENOIOCTLCMD ? -EINVAL : ret;

	/* Check media format and image resolution */
	if (dma->dma_format->code != fmt.format.code ||
	    dma->format.fmt.pix_mp.width != fmt.format.width ||
	    dma->format.fmt.pix_mp.height != fmt.format.height)
		return -EINVAL;

	return 0;
}

/* -----------------------------------------------------------------------------
 * videobuf2 queue operations
 */

/**
 * struct isp_dma_buffer - Video DMA buffer
 * @v4l2_buf: vb2_v4l2 buffer base object
 * @queue: buffer list entry in the DMA engine queued buffers list
 * @dma: DMA channel that uses the buffer
 */
struct isp_dma_buffer {
	struct vb2_v4l2_buffer v4l2_buf;
	struct list_head queue;
	struct isp_dma *dma;
};

#define to_isp_dma_buffer(vb) container_of(vb, struct isp_dma_buffer, v4l2_buf)

static void isp_dma_complete(void *param)
{
	struct isp_dma_buffer *buf = param;
	struct isp_dma *dma;
	struct v4l2_pix_format_mplane *pix_mp;
	int i, sizeimage;

	if (!buf || !buf->dma) {
		dev_err(dma->dev, "%s: error, invalid buffer", __FUNCTION__);
		return;
	}
	dma = buf->dma;
	pix_mp = &dma->format.fmt.pix_mp;

	spin_lock(&dma->queued_lock);
	//FIXME this check might be obsolete if race conditon is fixed
	if (buf->queue.prev != LIST_POISON2) {
		list_del(&buf->queue);
	} else {
		dev_dbg(dma->dev,
			"####### %s: buffer not in list, skip processing!!!\n",
			__FUNCTION__);
		spin_unlock(&dma->queued_lock);
		return;
	}
	spin_unlock(&dma->queued_lock);

	buf->v4l2_buf.field = V4L2_FIELD_NONE;
	buf->v4l2_buf.sequence = ++dma->sequence;
	buf->v4l2_buf.vb2_buf.timestamp = ktime_get_ns();

	for (i = 0; i < pix_mp->num_planes; i++) {
		sizeimage = pix_mp->plane_fmt[i].sizeimage;
		vb2_set_plane_payload(&buf->v4l2_buf.vb2_buf, i, sizeimage);
	}

	if (dma->transfer_done) {
		dma->transfer_done(dma->dev, &buf->v4l2_buf);
	}

	vb2_buffer_done(&buf->v4l2_buf.vb2_buf, VB2_BUF_STATE_DONE);
}

static int isp_dma_queue_setup(struct vb2_queue *vq, unsigned int *nbuffers,
			       unsigned int *nplanes, unsigned int sizes[],
			       struct device *alloc_devs[])
{
	struct isp_dma *dma = vb2_get_drv_priv(vq);

	if (isp_dma_is_metadata_channel(dma->channel)) {
		*nplanes = 1;
		sizes[0] = dma->metadata_buffersize;
	} else {
		struct v4l2_pix_format_mplane *pix_mp = &dma->format.fmt.pix_mp;
		int i;

		/* When called from VIDIOC_REQBUFS nplanes is set to 0 by caller and the
		* driver has to setup the buffer sizes. */
		if (*nplanes == 0) {
			*nplanes = pix_mp->num_planes;
			for (i = 0; i < pix_mp->num_planes; i++) {
				sizes[i] = pix_mp->plane_fmt[i].sizeimage;
			}
		}
		/* When called from VIDIOC_CREATE_BUFS nplanes is set by caller and the
		* driver has to check the buffer sizes. */
		else {
			if (*nplanes != pix_mp->num_planes)
				return -EINVAL;

			for (i = 0; i < *nplanes; i++) {
				if (sizes[i] < pix_mp->plane_fmt[i].sizeimage)
					return -EINVAL;
			}
		}
	}

	/* The amount of buffers is up to the application, so leave nbuffers
	 * as is. */

	return 0;
}

static int isp_dma_buffer_out_validate(struct vb2_buffer *vb)
{
	struct vb2_v4l2_buffer *vbuf = to_vb2_v4l2_buffer(vb);

	vbuf->field = V4L2_FIELD_NONE;

	return 0;
}

static int isp_dma_buffer_prepare(struct vb2_buffer *vb)
{
	struct vb2_v4l2_buffer *vbuf = to_vb2_v4l2_buffer(vb);
	struct isp_dma *dma = vb2_get_drv_priv(vb->vb2_queue);
	struct isp_dma_buffer *buf = to_isp_dma_buffer(vbuf);

	buf->dma = dma;

	return 0;
}

static int isp_dma_start_streaming(struct vb2_queue *vq, unsigned int count)
{
	struct isp_dma *dma = vb2_get_drv_priv(vq);
	struct isp_dma_buffer *buf, *nbuf;
	int ret;

	if (dma->type == ISP_DMA_TYPE_CAPTURE) {
		/* Start the DMA engine for capture before data is generated */
		dma_async_issue_pending(dma->channel);
	}

	/* Verify that the configured format matches the output of the connected
	 * subdev (does nothing in output mode, but can be called safely). */
	// FIXME: This does not work for the CSI2 device of ISP PRE
	// ret = isp_dma_verify_capture_format(dma);
	// if (ret < 0) {
	// 	dev_err(dma->dev, "Capture format does not match in sub-device\n");
	// 	goto error;
	// }

	ret = video_device_pipeline_start(&dma->video_dev, dma->pipe);
	if (ret < 0) {
		dev_err(dma->dev, "Video device pipeline start failed\n");
		goto error;
	}

	/* Signal that streaming has been started */
	if (dma->start_stream)
		ret = dma->start_stream(dma->dev, true);
	if (ret < 0) {
		dev_err(dma->dev, "'start_stream' callback failed\n");
		goto error_stop;
	}

	if (dma->type == ISP_DMA_TYPE_OUTPUT) {
		/* Start the DMA engine for output after pipeline was started */
		dma_async_issue_pending(dma->channel);
	}

	return 0;

error_stop:
	video_device_pipeline_stop(&dma->video_dev);

error:
	/* Return all queued buffers */
	spin_lock_irq(&dma->queued_lock);
	list_for_each_entry_safe(buf, nbuf, &dma->queued_bufs, queue) {
		vb2_buffer_done(&buf->v4l2_buf.vb2_buf, VB2_BUF_STATE_QUEUED);
		list_del(&buf->queue);
	}
	spin_unlock_irq(&dma->queued_lock);

	return ret;
}

static void isp_dma_stop_streaming(struct vb2_queue *vq)
{
	struct isp_dma *dma = vb2_get_drv_priv(vq);
	struct isp_dma_buffer *buf, *nbuf;

	/* Signal that streaming has been stopped */
	if (dma->start_stream)
		if (dma->start_stream(dma->dev, false) < 0)
			dev_err(dma->dev, "Stop stream callback failed");

	/* Stop DMA engine */
	dev_dbg(dma->dev, "## stop dma engine dmaengine_terminate_sync");
	dmaengine_terminate_sync(dma->channel);

	/* Cleanup the pipeline and mark it as being stopped. */
	video_device_pipeline_stop(&dma->video_dev);

	/* Return all queued buffers */
	spin_lock_irq(&dma->queued_lock);
	list_for_each_entry_safe(buf, nbuf, &dma->queued_bufs, queue) {
		vb2_buffer_done(&buf->v4l2_buf.vb2_buf, VB2_BUF_STATE_ERROR);
		list_del(&buf->queue);
	}
	spin_unlock_irq(&dma->queued_lock);
}

static void isp_dma_buffer_queue(struct vb2_buffer *vb)
{
	struct vb2_v4l2_buffer *vbuf = to_vb2_v4l2_buffer(vb);
	struct isp_dma_buffer *buf = to_isp_dma_buffer(vbuf);
	struct isp_dma *dma = vb2_get_drv_priv(vb->vb2_queue);
	struct scatterlist sgl[ISP_DMA_NUM_PLANES] = {};
	struct dma_async_tx_descriptor *desc;
	int i;

	/* Build the scatterlist, each plane is ony entry in the list. */
	for (i = 0; i < vb->num_planes; i++) {
		sgl[i].length = vbuf->planes[i].length;
		sgl[i].dma_address = vb2_dma_contig_plane_dma_addr(vb, i);
	}

	/* Note: usually the scatterlist has to be mapped with dma_map_sg().
	 * This is not needed in case of the ISP DMA since it only uses
	 * the scatterlist to pass the base addresses of multiple planes
	 * (each plane config is one entry in the scatterlist). It does not
	 * support real gather/scatter operations. */

	/* Prepare transaction descriptor */
	desc = dmaengine_prep_slave_sg(dma->channel, sgl, vb->num_planes,
				       dma->type == ISP_DMA_TYPE_CAPTURE ?
					       DMA_DEV_TO_MEM :
					       DMA_MEM_TO_DEV,
				       DMA_PREP_INTERRUPT | DMA_CTRL_ACK);
	if (!desc) {
		dev_err(dma->dev, "Failed to prepare DMA transfer\n");
		vb2_buffer_done(&buf->v4l2_buf.vb2_buf, VB2_BUF_STATE_ERROR);
		return;
	}
	desc->callback = isp_dma_complete;
	desc->callback_param = buf;

	/* Add buffer to queue of pending buffers */
	spin_lock_irq(&dma->queued_lock);
	list_add_tail(&buf->queue, &dma->queued_bufs);
	spin_unlock_irq(&dma->queued_lock);

	/* Submit descriptor to DMA engine */
	dmaengine_submit(desc);

	/* If pipeline is streaming, directly issue all pending transactions */
	if (vb2_is_streaming(&dma->queue)) {
		dma_async_issue_pending(dma->channel);
	}
}

static const struct vb2_ops isp_dma_queue_qops = {
	.queue_setup = isp_dma_queue_setup,
	.wait_prepare = vb2_ops_wait_prepare,
	.wait_finish = vb2_ops_wait_finish,
	.buf_out_validate = isp_dma_buffer_out_validate,
	.buf_prepare = isp_dma_buffer_prepare,
	.start_streaming = isp_dma_start_streaming,
	.stop_streaming = isp_dma_stop_streaming,
	.buf_queue = isp_dma_buffer_queue,
};

/* -----------------------------------------------------------------------------
 * V4L2 ioctls
 */

static int isp_dma_querycap(struct file *file, void *fh,
			    struct v4l2_capability *cap)
{
	struct isp_dma *dma = video_drvdata(file);

	/* V4L2 expects the device_caps field to not be changed and the
	 * capabilities field must be a superset of device_caps. Just leave
	 * them as is. */

	snprintf((char *)cap->driver, sizeof(cap->driver), "isp-dma-v4l2-%s",
		 (dma->type == ISP_DMA_TYPE_CAPTURE) ? "capture" : "output");
	strscpy((char *)cap->card, "Dream Chip ISP DMA", sizeof(cap->card));
	strscpy((char *)cap->bus_info, (char *)dma->video_dev.name,
		sizeof(cap->bus_info));

	return 0;
}

static int isp_dma_enum_format(struct file *file, void *fh,
			       struct v4l2_fmtdesc *f)
{
	struct isp_dma *dma = video_drvdata(file);
	int i, idx;

	/* Loop over all formats for the given DMA type */
	idx = -1;
	for (i = 0; i < isp_dma_num_formats; i++) {
		/* If format is supported, increment index */
		if (dma->format_supported[i])
			idx++;
		/* If format for requested index is found, return it */
		if (idx == f->index) {
			/* Note: Setting f->description here has no effect
			 * since it is overriden by the V4L2 framework
			 * afterwards. */
			f->flags = isp_dma_formats[i].flags;
			f->pixelformat = isp_dma_formats[i].fourcc;
			f->mbus_code = isp_dma_formats[i].code;
			return 0;
		}
	}

	/* In case end of list is reached, return -EINVAL according to spec */
	return -EINVAL;
}

static int __isp_dma_try_format(struct isp_dma *dma, struct v4l2_format *format,
				const struct isp_dma_format **ret_dma_format)
{
	const struct isp_dma_format *dma_format;
	struct v4l2_pix_format_mplane *pix_mp = &format->fmt.pix_mp;
	enum isp_dma_type type = V4L2_TYPE_IS_CAPTURE(format->type) ?
					 ISP_DMA_TYPE_CAPTURE :
					 ISP_DMA_TYPE_OUTPUT;
	u32 byte_per_pixel, stride, min_stride;
	int i, active_planes = 0;

	/* Check type is correct */
	if (type != dma->type) {
		dev_err(dma->dev,
			"Format type %d and DMA type %d do not match\n", type,
			dma->type);
		return -EINVAL;
	}

	/* Interlaced formats are not supported */
	// Override "ANY" field setting with "NONE"
	if (pix_mp->field == V4L2_FIELD_ANY)
		pix_mp->field = V4L2_FIELD_NONE;
	if (pix_mp->field != V4L2_FIELD_NONE) {
		dev_err(dma->dev, "Interlaced video is not supported\n");
		return -EINVAL;
	}

	/* Check if format is in format list and supported by the remote device */
	for (i = 0; i < isp_dma_num_formats; i++) {
		if (isp_dma_formats[i].fourcc == pix_mp->pixelformat) {
			if (dma->format_supported[i])
				dma_format = &isp_dma_formats[i];
			else {
				dev_err(dma->dev,
					"Pixel format is supported by DMA, but not by remote device\n");
				return -EINVAL;
			}
		}
	}
	if (!dma_format) {
		dev_err(dma->dev, "Pixel format not supported (%08x)\n",
			pix_mp->pixelformat);
		return -EINVAL;
	}

	/* Get number of active planes */
	for (i = 0; i < ISP_DMA_NUM_PLANES; i++) {
		if (dma_format->comp_sizes[i] > 0 &&
		    !dma_format->dummy_comp[i]) {
			active_planes++;
		}
	}

	/* Check that the amount of planes is correct */
	// Override number of planes if set to 0
	if (pix_mp->num_planes == 0)
		pix_mp->num_planes = active_planes;
	if (pix_mp->num_planes != active_planes) {
		dev_err(dma->dev,
			"Number of planes %d does not match expectation %d\n",
			pix_mp->num_planes, active_planes);
		return -EINVAL;
	}

	/* Check the colorspace */
	// Override default with actual colorspace from DMA format
	pix_mp->colorspace = dma_format->colorspace;

	pix_mp->field = dma_format->field;

	/* Width and height may be adjusted to what is supported by the HW */
	pix_mp->width =
		clamp(pix_mp->width, ISP_DMA_MIN_WIDTH, ISP_DMA_MAX_WIDTH);
	pix_mp->height =
		clamp(pix_mp->height, ISP_DMA_MIN_HEIGHT, ISP_DMA_MAX_HEIGHT);

	/* Calculate the minimum stride and override stride if too small */
	for (i = 0; i < active_planes; i++) {
		// Make sure stride is 256 byte aligned
		stride = (pix_mp->plane_fmt[i].bytesperline + 255) & ~255;
		pix_mp->plane_fmt[i].bytesperline = stride;
		// Pixel size is rounded up to the nearest byte boundary
		byte_per_pixel = (dma_format->comp_sizes[i] + 7) >> 3;
		// Calculate minimum stride
		min_stride = (pix_mp->width * byte_per_pixel + 255) & ~255;
		// Check if resulting stride is bigger than maximum stride
		if (min_stride > ISP_DMA_MAX_STRIDE) {
			dev_err(dma->dev,
				"Resulting stride (bytesperline) %d is too big\n",
				stride);
			return -EINVAL;
		}
		// Make sure bytesperline is big enough and set the image size
		stride = max(stride, min_stride);
		pix_mp->plane_fmt[i].bytesperline = stride;
		pix_mp->plane_fmt[i].sizeimage = stride * pix_mp->height;
	}

	/* Return DMA format if valid pointer is given */
	if (ret_dma_format)
		*ret_dma_format = dma_format;

	return 0;
}

static int isp_dma_try_format(struct file *file, void *fh,
			      struct v4l2_format *format)
{
	struct isp_dma *dma = video_drvdata(file);
	/* This is just a wrapper for our internal try format function which
	 * can optionally also return the DMA format that matches the given
	 * given format description. */
	return __isp_dma_try_format(dma, format, NULL);
}

static int isp_dma_get_format(struct file *file, void *fh,
			      struct v4l2_format *format)
{
	struct isp_dma *dma = video_drvdata(file);
	enum isp_dma_type type = V4L2_TYPE_IS_CAPTURE(format->type) ?
					 ISP_DMA_TYPE_CAPTURE :
					 ISP_DMA_TYPE_OUTPUT;

	/* Check type is correct */
	if (type != dma->type) {
		dev_err(dma->dev,
			"Format type %d and DMA type %d do not match\n", type,
			dma->type);
		return -EINVAL;
	}

	format->fmt.pix_mp = dma->format.fmt.pix_mp;
	return 0;
}

static int __isp_dma_set_format(struct isp_dma *dma, struct v4l2_format *format)
{
	const struct isp_dma_format *dma_format;
	enum isp_dma_type type = V4L2_TYPE_IS_CAPTURE(format->type) ?
					 ISP_DMA_TYPE_CAPTURE :
					 ISP_DMA_TYPE_OUTPUT;
	struct dma_slave_config config = {};
	struct isp_dma_channel_config peripheral_config = {};
	struct v4l2_pix_format_mplane *pix_mp = &format->fmt.pix_mp;
	struct v4l2_subdev *subdev;
	struct v4l2_subdev_format fmt;
	int i, ret;

	/* Check type is correct */
	if (type != dma->type) {
		dev_err(dma->dev,
			"Format type %d and DMA type %d do not match\n", type,
			dma->type);
		return -EINVAL;
	}

	/* Check if queue is busy */
	if (vb2_is_busy(&dma->queue))
		return -EBUSY;

	/* Check if format is valid */
	ret = __isp_dma_try_format(dma, format, &dma_format);
	if (ret < 0)
		return ret;

	/* Get connected device */
	subdev = isp_dma_remote_subdev(&dma->pad, &fmt.pad);
	if (subdev) {
		fmt.which = V4L2_SUBDEV_FORMAT_ACTIVE;
		fmt.format.width = format->fmt.pix_mp.width;
		fmt.format.height = format->fmt.pix_mp.height;
		fmt.format.colorspace = dma_format->colorspace;
		fmt.format.code = dma_format->code;
		fmt.format.field = dma_format->field;

		ret = v4l2_subdev_call(subdev, pad, set_fmt, NULL, &fmt);
		if (ret < 0)
			dev_warn(dma->dev,
				 "Failed to set remote subdev format: %d", ret);
	}

	/* Build DMA engine configuration */
	// For now always disable multi-lane mode
	/* TODO:
	 *
	 * ISP PRE uses single-lane mode when the sensor provides multiple
	 * exposures pixel-interleaved in a single virtual channel (so the MIPI
	 * filter can not separate them). Multi-lane mode is used when each
	 * exposure is send in a separate virtual channel and the MIPI filter
	 * can map each channel to a separate lane.
	 *
	 * ISP MAIN uses multi-lane mode when doing direct IO (DMA Write -> DMA
	 * Read, not via memory) as in this case the sensor must deliver the
	 * different exposures line interleaved as the RPP does not have a full
	 * frame buffer at its input, only a line buffer. When reading from
	 * memory the DMA can read all exposures at the same time and deliver
	 * them to the RPP, so in this case single-lane mode is used.
	 *
	 * For now we only support one ISP PRE format which already does the
	 * fusion in the sensor and we always go over the memory to ISP MAIN, so
	 * we always use single-lane mode. */
	peripheral_config.multi_lane = false;
	peripheral_config.mode_420 = dma_format->mode_420;

	for (i = 0; i < 4; i++) {
		struct isp_dma_plane_config *plane =
			&peripheral_config.planes[i];
		plane->comp_size = dma_format->comp_sizes[i];
		plane->stride = pix_mp->plane_fmt[i].bytesperline;
		plane->width = pix_mp->width;
		plane->height = pix_mp->height;
		plane->dummy = dma_format->dummy_comp[i];
	}
	// Register peripheral config in general config struct
	config.peripheral_config = &peripheral_config;
	config.peripheral_size = sizeof(peripheral_config);

	/* Set DMA engine configuration */
	ret = dmaengine_slave_config(dma->channel, &config);
	if (ret < 0) {
		/* Slave configuration should not fail if __isp_dma_try_format()
		 * is implemented correctly!*/
		dev_err(dma->dev, "Set DMA slave config failed with %d", ret);
		return ret;
	}

	/* Store V4L2 format and DMA format in DMA context */
	dma->format.fmt.pix_mp = *pix_mp;
	dma->dma_format = dma_format;

	return 0;
}

static int isp_dma_set_format(struct file *file, void *fh,
			      struct v4l2_format *format)
{
	struct isp_dma *dma = video_drvdata(file);
	/* This is just a wrapper for our internal try format function which
	 * can optionally also return the DMA format that matches the given
	 * given format description. */
	return __isp_dma_set_format(dma, format);
}

static int isp_dma_enum_fmt_meta_cap(struct file *file, void *priv,
				     struct v4l2_fmtdesc *f)
{
	struct video_device *video = video_devdata(file);
	struct isp_dma *dma = video_drvdata(file);

	if (f->index > 0 || f->type != video->queue->type)
		return -EINVAL;

	f->pixelformat = dma->format.fmt.meta.dataformat;
	return 0;
}

static int isp_dma_g_fmt_meta_cap(struct file *file, void *priv,
				  struct v4l2_format *f)
{
	struct video_device *video = video_devdata(file);
	struct isp_dma *dma = video_drvdata(file);
	struct v4l2_meta_format *meta = &f->fmt.meta;
	struct isp_dma_channel_config peripheral_config = {};
	struct dma_slave_config config = {};
	int ret;

	if (f->type != video->queue->type)
		return -EINVAL;

	peripheral_config.multi_lane = false;
	peripheral_config.mode_420 = false;

	/* Set comp_size to 32 bit, this is fine for both DPCC and HIST output
	 * since DPCC already outputs 32 bit words and HIST outputs 24 bits
	 * and we would like to receive them aligned to 32 bit. The samples will
	 * be MSB aligned but that is better than extracting the data from
	 * 24 bit packed. */
	/* There is a condition we need to meet to make the DMA work properly
	 * with UNKNOWN_SIZE_MODE:
	 *
	 * The horizontal size in pixel must be equal to the
	 * cfg_mem_line_size/’burstsize in pixel’, so that the
	 * used buffer does not contain any holes.
	 *
	 * We realize that by configuring that 1 line is 1 burst. */
	peripheral_config.planes[0].comp_size = 32;
	peripheral_config.planes[0].stride = 256; // 1 line = 1 burst
	peripheral_config.planes[0].width = peripheral_config.planes[0].stride >> 2;
	peripheral_config.planes[0].height = dma->format.fmt.meta.buffersize >> 8;
	peripheral_config.planes[0].dummy = false;

	// Register peripheral config in general config struct
	config.peripheral_config = &peripheral_config;
	config.peripheral_size = sizeof(peripheral_config);

	/* Set DMA engine configuration */
	ret = dmaengine_slave_config(dma->channel, &config);
	if (ret < 0) {
		/* Slave configuration should not fail if __isp_dma_try_format()
		 * is implemented correctly!*/
		dev_err(dma->dev, "Set DMA slave config failed with %d", ret);
		return ret;
	}

	*meta = dma->format.fmt.meta;
	meta->buffersize = dma->metadata_buffersize;

	return 0;
}

static const struct v4l2_ioctl_ops isp_dma_ioctl_ops[ISP_DMA_TYPE_NUM] = {
	{
		// CAPTURE
		.vidioc_querycap = isp_dma_querycap,
		.vidioc_enum_fmt_vid_cap = isp_dma_enum_format,
		.vidioc_g_fmt_vid_cap_mplane = isp_dma_get_format,
		.vidioc_s_fmt_vid_cap_mplane = isp_dma_set_format,
		.vidioc_try_fmt_vid_cap_mplane = isp_dma_try_format,
		.vidioc_reqbufs = vb2_ioctl_reqbufs,
		.vidioc_querybuf = vb2_ioctl_querybuf,
		.vidioc_qbuf = vb2_ioctl_qbuf,
		.vidioc_dqbuf = vb2_ioctl_dqbuf,
		.vidioc_create_bufs = vb2_ioctl_create_bufs,
		.vidioc_expbuf = vb2_ioctl_expbuf,
		.vidioc_streamon = vb2_ioctl_streamon,
		.vidioc_streamoff = vb2_ioctl_streamoff,
		.vidioc_enum_fmt_meta_cap = isp_dma_enum_fmt_meta_cap,
		.vidioc_g_fmt_meta_cap = isp_dma_g_fmt_meta_cap,
		.vidioc_s_fmt_meta_cap = isp_dma_g_fmt_meta_cap,
		.vidioc_try_fmt_meta_cap = isp_dma_g_fmt_meta_cap,
	},
	{
		// OUTPUT
		.vidioc_querycap = isp_dma_querycap,
		.vidioc_enum_fmt_vid_out = isp_dma_enum_format,
		.vidioc_g_fmt_vid_out_mplane = isp_dma_get_format,
		.vidioc_s_fmt_vid_out_mplane = isp_dma_set_format,
		.vidioc_try_fmt_vid_out_mplane = isp_dma_try_format,
		.vidioc_reqbufs = vb2_ioctl_reqbufs,
		.vidioc_querybuf = vb2_ioctl_querybuf,
		.vidioc_qbuf = vb2_ioctl_qbuf,
		.vidioc_dqbuf = vb2_ioctl_dqbuf,
		.vidioc_create_bufs = vb2_ioctl_create_bufs,
		.vidioc_expbuf = vb2_ioctl_expbuf,
		.vidioc_streamon = vb2_ioctl_streamon,
		.vidioc_streamoff = vb2_ioctl_streamoff,
	},
};

/**
 * Needs to be implemented even that we have no real initialization
 * work to do. When we use the default v4l2 implementatíon of the
 * file open/release another (independent) user like a second gstreamer
 * pipeline or even a trivial call to gst-inspect would cause our
 * first one to be stopped and closed.
 */
static int isp_dma_open(struct file *file)
{
	struct isp_dma *dma = video_drvdata(file);
	int ret;

	dev_dbg(dma->dev, "%s\n", __func__);

	if (mutex_lock_interruptible(&dma->lock))
		return -ERESTARTSYS;

	ret = v4l2_fh_open(file);
	if (ret)
		goto done_open;
	if (v4l2_fh_is_singular_file(file)) {
		// /* First open */
		dev_dbg(dma->dev, "%s is_first\n", __func__);
	}

done_open:
	mutex_unlock(&dma->lock);
	dev_dbg(dma->dev, "%s done\n", __func__);

	return ret;
}

static int isp_dma_release(struct file *file)
{
	struct isp_dma *dma = video_drvdata(file);
	bool is_last;

	dev_dbg(dma->dev, "%s\n", __func__);

	mutex_lock(&dma->lock);
	is_last = v4l2_fh_is_singular_file(file);
	_vb2_fop_release(file, NULL);
	if (is_last) {
		/* Last close */
		dev_dbg(dma->dev, "%s is_last\n", __func__);
	}
	mutex_unlock(&dma->lock);

	return 0;
}

/* -----------------------------------------------------------------------------
 * V4L2 file operations
 */

static const struct v4l2_file_operations isp_dma_fops = {
	.owner = THIS_MODULE,
	.unlocked_ioctl = video_ioctl2,
	.open = isp_dma_open,
	.release = isp_dma_release,
	.poll = vb2_fop_poll,
	.mmap = vb2_fop_mmap,
};

/* -----------------------------------------------------------------------------
 * ISP Video DMA Core
 */

int isp_dma_init(struct device *dev, struct isp_dma *dma,
		 const char *channel_name, struct v4l2_device *v4l2_dev,
		 struct media_pipeline *pipe,
		 int (*start_stream)(struct device *, bool),
		 void (*transfer_done)(struct device *,
				       struct vb2_v4l2_buffer *v4l2_buf))
{
	enum dma_transfer_direction dir;
	bool metadata_channel;
	int video_dev_nr;
	int ret, i;

	/* Check parameters */
	if (!dev) {
		dev_err(dev,
			"No valid device pointer passed to isp_dma_init()\n");
		return -EINVAL;
	}
	if (!v4l2_dev || !dma || !pipe || !channel_name) {
		dev_err(dev,
			"No valid v4l2_dev, dma, pipe pointer or channel name passed\n");
		return -EINVAL;
	}

	/* Initialize ISP DMA structure */
	dma->dev = dev;
	dma->v4l2_dev = v4l2_dev;
	dma->pipe = pipe;
	dma->start_stream = start_stream;
	dma->transfer_done = transfer_done;
	mutex_init(&dma->lock);
	INIT_LIST_HEAD(&dma->queued_bufs);
	spin_lock_init(&dma->queued_lock);

	/* Get the DMA channel */
	dma->channel = dma_request_chan(dev, channel_name);
	if (IS_ERR(dma->channel)) {
		ret = PTR_ERR(dma->channel);
		if (ret != -EPROBE_DEFER)
			dev_err(dev,
				"DMA channel '%s' not found, check device tree\n",
				channel_name);
		goto error;
	}

	/* Get transfer direction and derive DMA type from it */
	dir = isp_dma_get_transfer_direction(dma->channel);
	dma->type = dir == DMA_DEV_TO_MEM ? ISP_DMA_TYPE_CAPTURE :
					    ISP_DMA_TYPE_OUTPUT;

	metadata_channel = isp_dma_is_metadata_channel(dma->channel);

	/* Setup media pad */
	dma->pad.flags = (dma->type == ISP_DMA_TYPE_CAPTURE) ?
				 MEDIA_PAD_FL_SINK :
				 MEDIA_PAD_FL_SOURCE;

	/* Allocate memory for the "format supported flags" for this
	 * channel / pad. By default, assume all formats are supported. */
	dma->format_supported =
		kmalloc_array(isp_dma_num_formats, sizeof(bool), GFP_KERNEL);
	if (!dma->format_supported) {
		ret = -ENOMEM;
		goto error;
	}
	for (i = 0; i < isp_dma_num_formats; i++) {
		dma->format_supported[i] = true;
	}

	/* Initialize media pads */
	ret = media_entity_pads_init(&dma->video_dev.entity, 1, &dma->pad);
	if (ret < 0) {
		dev_err(dev, "Failed to init media entity pad\n");
		goto error;
	}

	/* Setup the video node */
	dma->video_dev.v4l2_dev = dma->v4l2_dev;
	dma->video_dev.fops = &isp_dma_fops;
	dma->video_dev.queue = &dma->queue;
	snprintf(dma->video_dev.name, sizeof(dma->video_dev.name), "%pOFn:%s",
		 dev->of_node,
		 dma->type == ISP_DMA_TYPE_CAPTURE ? "capture" : "output");

	dma->video_dev.vfl_type = VFL_TYPE_VIDEO;
	dma->video_dev.device_caps = V4L2_CAP_STREAMING;
	switch (dma->type) {
	case ISP_DMA_TYPE_CAPTURE:
		dma->video_dev.vfl_dir = VFL_DIR_RX;
		dma->video_dev.device_caps |=
			metadata_channel ? V4L2_CAP_META_CAPTURE :
					   V4L2_CAP_VIDEO_CAPTURE_MPLANE;
		dma->queue.type = metadata_channel ?
					  V4L2_BUF_TYPE_META_CAPTURE :
					  V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
		break;
	case ISP_DMA_TYPE_OUTPUT:
		dma->video_dev.vfl_dir = VFL_DIR_TX;
		dma->video_dev.device_caps |=
			metadata_channel ? V4L2_CAP_META_OUTPUT :
					   V4L2_CAP_VIDEO_OUTPUT_MPLANE;
		dma->queue.type = metadata_channel ?
					  V4L2_BUF_TYPE_META_OUTPUT :
					  V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
		break;
	default:
		dev_err(dev, "Unexpected DMA type: %d\n", dma->type);
		goto error;
	};

	dma->video_dev.release = video_device_release_empty;
	dma->video_dev.ioctl_ops = &isp_dma_ioctl_ops[dma->type];
	dma->video_dev.lock = &dma->lock;

	video_set_drvdata(&dma->video_dev, dma);

	/* Setup the buffer queue */
	dma->queue.io_modes = VB2_MMAP | VB2_DMABUF;
	dma->queue.lock = &dma->lock;
	dma->queue.drv_priv = dma;
	dma->queue.buf_struct_size = sizeof(struct isp_dma_buffer);
	dma->queue.ops = &isp_dma_queue_qops;
	dma->queue.mem_ops = &vb2_dma_contig_memops;
	dma->queue.timestamp_flags = V4L2_BUF_FLAG_TIMESTAMP_MONOTONIC |
				     V4L2_BUF_FLAG_TSTAMP_SRC_EOF;
	dma->queue.dev = dev;

	ret = vb2_queue_init(&dma->queue);
	if (ret < 0) {
		dev_err(dev, "Failed to initialize VB2 queue\n");
		goto error;
	}
	dma->video_dev.queue = &dma->queue;

	/* Finally register the video device using the video device number that
	 * was specified in the device tree for the DMA channel. */
	video_dev_nr = isp_dma_get_video_device_number(dma->channel);
	ret = video_register_device(&dma->video_dev, VFL_TYPE_VIDEO,
				    video_dev_nr);
	if (ret < 0) {
		dev_err(dev, "failed to register video device\n");
		goto error;
	}

	return 0;

error:
	isp_dma_cleanup(dma);
	return ret;
}
EXPORT_SYMBOL(isp_dma_init);

void isp_dma_cleanup(struct isp_dma *dma)
{
	if (video_is_registered(&dma->video_dev))
		video_unregister_device(&dma->video_dev);

	if (!IS_ERR_OR_NULL(dma->channel))
		dma_release_channel(dma->channel);
	if (dma->format_supported)
		kfree(dma->format_supported);

	media_entity_cleanup(&dma->video_dev.entity);

	mutex_destroy(&dma->lock);
}
EXPORT_SYMBOL(isp_dma_cleanup);

int isp_dma_set_supported_formats(struct isp_dma *dma, u32 *fourcc, size_t len)
{
	int i, j;
	bool supported;
	int num_supported;

	// Loop over all DMA formats
	for (i = 0; i < isp_dma_num_formats; i++) {
		supported = false;
		// Loop over given supported format list
		for (j = 0; j < len; j++) {
			// Check if formats are identical
			if (isp_dma_formats[i].fourcc == fourcc[j]) {
				supported = true;
				num_supported++;
				break;
			}
		}
		// Update "format supported" entry
		dma->format_supported[i] = supported;
	}

	/* If not all formats in the fourcc list were found, the caller used
	 * formats which are not known by the DMA. */
	if (num_supported != len) {
		dev_err(dma->dev,
			"Supported format list contains unkown formats");
		return -EINVAL;
	}

	return 0;
}
EXPORT_SYMBOL(isp_dma_set_supported_formats);

int isp_dma_override_format(struct isp_dma *dma, struct v4l2_format *format)
{
	return __isp_dma_set_format(dma, format);
}
EXPORT_SYMBOL(isp_dma_override_format);

u64 isp_dma_get_dma_mask(struct isp_dma *dma)
{
	return dma_get_mask(dma->channel->device->dev);
}
EXPORT_SYMBOL(isp_dma_get_dma_mask);

int isp_dma_set_metadata(struct isp_dma *dma, u32 dataformat, u32 buffersize,
			 u32 transfersize)
{
	struct v4l2_meta_format *meta = &dma->format.fmt.meta;

	if (!isp_dma_is_metadata_channel(dma->channel)) {
		return -EINVAL;
	}

	dma->metadata_buffersize = buffersize;
	meta->dataformat = dataformat;
	meta->buffersize = (transfersize + 255) & ~255;

	if (dma->metadata_buffersize < meta->buffersize) {
		dma->metadata_buffersize = meta->buffersize;
	}

	return 0;
}
EXPORT_SYMBOL(isp_dma_set_metadata);
