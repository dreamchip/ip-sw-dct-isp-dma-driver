/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 */

#ifndef _ISP_DMA_H_
#define _ISP_DMA_H_

#include <linux/types.h>

/* Forward declarations */
struct dma_chan;

/* Definitions */
#define ISP_DMA_MIN_WIDTH 1u // Minimum width in pixels
#define ISP_DMA_MAX_WIDTH (u32)((1 << 14) - 1) // Maximum width in pixels
#define ISP_DMA_MIN_HEIGHT 1u // Minimum height in lines
#define ISP_DMA_MAX_HEIGHT (u32)((1 << 14) - 1) // Maximum height in lines
#define ISP_DMA_MAX_STRIDE (u32)(((1 << 10) - 1) << 8) // Maximum stride in byte

/**
 * Glossary:
 *
 * Channel: A channel refers to a DMA channel as used by the dmaengine,
 *          that means it is one of the HW channels provided by a DMA
 *          controller. In case of the ISP DMA each channel is a separate
 *          HW unit with its own set of configuration registers and
 *          interrupts.
 *          ISP DMA channels can either be Read Channels (reading from
 *          memory, DMA direction MEM_TO_DEV) or Write Channels (writing
 *          to memory, DMA direction DEV_TO_MEM). The direction is fixed
 *          in HW, defined in the device tree and can not be changed.
 *
 * Component: The ISP DMA mainly deals with image data. In this case a
 *            component means a component of an input pixel (e.g. red, green
 *            and blue color components). When the ISP DMA is used to
 *            transmit generic data (e.g. measurement data) a component
 *            may also just be a byte or a data word.
 *            Components define how the ISP DMA interprets the data it
 *            transmits.
 *
 * Plane: A plane is a contiguous block of memory where the DMA stores
 *        data or fetches data from. Depending on the configuration an ISP
 *        DMA channel can write to or read data from up to 4 planes.
 *
 * Lane: The ISP DMA can receive or send data on one or multiple lanes.
 *       In multi-lane mode each lane represents one of the components
 *       which is flagged by a separate HW sideband signal. In case of
 *       the Write DMA this sideband signal must be provided by the sender,
 *       so that the DMA can correctly decode in the input data. For the
 *       Read DMA the sideband signal is emitted by the DMA.
 *       In single-lane mode all components are send or received interleaved
 *       in the same lane.
 */

/**
 * Components can either be stored in a single plane in memory
 * or in one plane per component, therefore the maximum number
 * of planes is identical to the maximum number of supported
 * components which is 4.
 */
#define ISP_DMA_NUM_PLANES 4

/**
 * struct isp_dma_plane_config - ISP DMA plane configuration
 * @comp_size: Sizes of a single component in bit, valid range [0, 48].
 * @stride: DMA stride (memory line size) in byte. Must be >= width *
 *          byte_per_comp and burst aligned to 256 byte.
 *          This is a in/out parameter, if a stride of 0 is passed the
 *          driver will calculate and return the minimum stride. Valid
 *          range [0 (auto), 261888].
 * @width: Image width in components (e.g. pixels), valid range [1, 16383]
 * @height: Image height in lines, valid range [1, 16383]
 * @dummy: Denotes that this plane should be skipped for DMA
 *
 * A component can either be a single pixel component or a set of
 * interleaved pixel components.
 * Example: When a 24 bit RGB image is stored in a single plane,
 * it has a single RGB component with a size of 24 bit. If it is
 * stored in separate planes, it consists of 3 components (R, G, B)
 * each with a size of 8 bit.
 *
 * For calculation of the stride keep in mind that the component size is
 * always rounded up to the next byte boundary, e.g. a pixel with 20 bit is
 * stored LSB aligned in three bytes:
 *
 * byte_per_comp = (comp_size + 7) >> 3
 *               = (20 + 7) >> 3
 *               = 3
 *
 * So for an example the minimum stride for a plane with 20 bit components
 * and 1920 components per line is (aligned to 256 byte):
 *
 * min_stride = (byte_per_comp * width + 255) & ~255
 * 	      = (3 * 1920 + 255) & ~255
 *            = 5888
 *
 * This calculation can be done automatically by the driver if a stride
 * of 0 is specified which will then be overriden by the driver with the
 * minimum stride.
 */
struct isp_dma_plane_config {
	u8 comp_size;
	u16 stride;
	u16 width;
	u16 height;
	bool dummy;
};

/**
 * struct isp_dma_channel_config - ISP DMA channel configuration structure
 * @multi_lane: Set to enable multi-lane mode where each component
 *              is received / transmitted in a separate lane flagged by
 * 		a HW sideband signal.
 *
 * @mode_420: Enable special 4:2:0 semi-planar mode. Requires comp_size[0]
 *            and comp_size[1] to be set > 0. Data for plane 1 must
 *            have half the width and height of data for plane 0.
 *
 * @direct_mode
 * @planes: Array of planes, for details see isp_dma_plane_config.
 *
 * Pass this structure to a DCT ISP DMA engine during
 * dmaengine_slave_config() using the
 * dma_slave_config->peripheral_config pointer.
 *
 * The DMA has two operational modes:
 * - Single-lane mode: A pixel contains multiple components which are
 *   concatenated and received or transmitted in a single lane. Component
 *   0 is placed on the MSBs with comp_size[0] followed by component 1
 *   with comp_size[1] bits and so on.
 *   In this mode the sum of the component sizes must not exceed 48 bit!
 * - Multi-lane mode: Components are received / transmitted in separate
 *   lanes. For a Write channel the sender must flag each component using
 *   a HW sideband signal, for a Read channel the DMA generates the
 *   according output signals. Each lane may have a component size of up
 *   to 48 bit.
 *
 * Additionally there is the special 4:2:0 semi-planar mode which only works
 * with exactly two components which are send / received in two planes. In
 * this mode Luma data is expected on component 0 and the interleaved color
 * data on component 1. Luma data is written / read for every line while
 * color data will only be written / read in odd lines. That means the color
 * plane must have half the width and height of the luma plane.
 *
 * Each component can have a different pixel component size given by
 * planes.comp_size[0] to planes.comp_size[DCT_ISP_DMA_NUM_CHN - 1].
 * If a component is not used, the size must be set to 0.
 * Please note that components can not be skipped, that means if comp_size[1]
 * is set to 0, comp_size[2] to comp_size[DCT_ISP_DMA_NUM_CHN - 1] must also
 * be 0.
 *
 * Read channel limitation:
 * In cases of the Read channel the planes may have different strides, but
 * all planes must have the same image dimensions, that means chn_cfg[].width
 * and chn_cfg[].height must be set to the same values for all active planes
 * (except for 4:2:0 mode where the second plane must have half the
 * dimensions of the first plane, see above).
 */
struct isp_dma_channel_config {
	bool multi_lane;
	bool mode_420;
	int active_planes;
	struct isp_dma_plane_config planes[ISP_DMA_NUM_PLANES];
};

/**
 * isp_dma_get_transfer_direction - Get transfer direction of an ISP DMA channel
 * @chan: DMA channel which is contained by a isp_dma_channel structure.
 *
 * Return: DMA transfer direction.
 *
 * The transfer direction of an ISP DMA channel is fixed in HW and can not be
 * changed. Use this function to query the direction and check if it matches the
 * required transfer direction.
 */
enum dma_transfer_direction
isp_dma_get_transfer_direction(struct dma_chan *chan);

/**
 * isp_dma_get_video_device_number - Get the video device number
 * @chan: DMA channel which is contained by a isp_dma_channel structure.
 *
 * Return: Video device number for this channel.
 *
 * The video device number can be used as a hint to video_register_device()
 * to create a /dev/video device with a deterministic numbering.
 *
 * If no number was provided via the device tree -1 is returned (which
 * video_register_device() accepts as "auto").
 */
int isp_dma_get_video_device_number(struct dma_chan *chan);

/**
 * isp_dma_is_metadata_channel - Check if DMA channel is for metadata
 * @chan: DMA channel which is contained by a isp_dma_channel structure.
 *
 * Return: true if channel transports metadata instead of video data
 */
bool isp_dma_is_metadata_channel(struct dma_chan *chan);

/**
 * isp_dma_is_direct_mode - Check if DMA channel is configured for direct-mode
 * @chan: DMA channel which is contained by a isp_dma_channel structure.
 *
 * Return: true if channel is directly connected to an isp_pre instance
 */
bool isp_dma_is_direct_mode(struct dma_chan *chan);

/**
 * isp_dma_get_direct_source - Get pointer to connected direct-mode source
 * if available.
 * @chan: DMA channel which is contained by a dma_chan structure.
 *
 * Return: NULL if no source is available, otherwise a valid pointer
 */
struct platform_device *isp_dma_get_direct_source(struct dma_chan *chan);

#endif //_ISP_DMA_H_
