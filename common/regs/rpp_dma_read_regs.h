/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 *
 * Automatically generated by sig2 v0.62.2
 * source: rpp_dma_read_regs.txt, svn r5955
 */
#ifndef __RPP_DMA_READ_REGS_H__
#define __RPP_DMA_READ_REGS_H__


#define DMA_READ_ID_REG 0x00000000
#define DMA_READ_CTRL_REG 0x00000004
#define DMA_READ_CFG_REG 0x00000008
#define DMA_READ_CFG_IN_SIZE_REG 0x0000000C
#define DMA_ARUSER_REG 0x00000010
#define DMA_READ_BA0_REG 0x00000014
#define DMA_READ_BA1_REG 0x00000018
#define DMA_READ_BA2_REG 0x0000001C
#define DMA_READ_BA3_REG 0x00000020
#define DMA_READ_MEM_LINE_SIZE0_REG 0x00000024
#define DMA_READ_MEM_LINE_SIZE1_REG 0x00000028
#define DMA_READ_MEM_LINE_SIZE2_REG 0x0000002C
#define DMA_READ_MEM_LINE_SIZE3_REG 0x00000030
#define DMA_READ_PIC_SIZE_REG 0x00000034

/* DMA_READ_ID_REG */
// Module written by DCT.
#define DCT_ID_MASK 0xFF000000U
#define DCT_ID_SHIFT 24U
// Project.
#define PROJECT_ID_MASK 0x00FF0000U
#define PROJECT_ID_SHIFT 16U
// Revision.
#define REVISION_MASK 0x0000FF00U
#define REVISION_SHIFT 8U
// Module ID.
#define MODULE_ID_MASK 0x000000FFU
#define MODULE_ID_SHIFT 0U

/* DMA_READ_CTRL_REG */
// Generates a configuration update at the end of the actual frame.
// Leads to an update of the shadow registers after last programming.
#define DMA_READ_GEN_CFG_UPD_MASK 0x00000020U
#define DMA_READ_GEN_CFG_UPD_BIT 5
// Forced configuration update. Leads to an immediate update of the shadow registers.
#define DMA_READ_CFG_UPD_MASK 0x00000010U
#define DMA_READ_CFG_UPD_BIT 4
// Soft Reset: Sets internal registers and buffers to reset value.
// The configuration registers themselves are not affected.
#define SOFT_RESET_MASK 0x00000001U
#define SOFT_RESET_BIT 0

/* DMA_READ_CFG_REG */
// get data from AXI slave port. The base address (register DMA_READ_BAx[11:0]) is compared
// with the AXI address to define the plane respective channel ID.
#define DMA_READ_AXI_SLAVE_MODE_MASK 0x00000400U
#define DMA_READ_AXI_SLAVE_MODE_BIT 10
// multi channel mode, The channel are multiplexed and output with channel identifier.
#define DMA_READ_MULTI_CHANNEL_MODE_MASK 0x00000200U
#define DMA_READ_MULTI_CHANNEL_MODE_BIT 9
// YUV 4:2:0 mode: write y data in even lines and Y+UV in odd lines
#define DMA_READ_420_MODE_MASK 0x00000100U
#define DMA_READ_420_MODE_BIT 8
// set the number of planes used in this usecase
#define CFG_NUM_PLANES_MASK 0x00000070U
#define CFG_NUM_PLANES_SHIFT 4U
// plane 3: Enables dma for plane 3, only if cfg_num_planes = 4
#define DMA_READ_ENABLE3_MASK 0x00000008U
#define DMA_READ_ENABLE3_BIT 3
// plane 2: Enables dma for plane 2, only if cfg_num_planes >= 3
#define DMA_READ_ENABLE2_MASK 0x00000004U
#define DMA_READ_ENABLE2_BIT 2
// plane 1: Enables dma for plane 1, only if cfg_num_planes >= 2
#define DMA_READ_ENABLE1_MASK 0x00000002U
#define DMA_READ_ENABLE1_BIT 1
// plane 0: Enables dma for plane 0, only if cfg_num_planes >= 1
#define DMA_READ_ENABLE0_MASK 0x00000001U
#define DMA_READ_ENABLE0_BIT 0

/* DMA_READ_CFG_IN_SIZE_REG */
// pixel width of plane 3 [1..48]
#define DMA_READ_PXL_WIDTH3_MASK 0x3F000000U
#define DMA_READ_PXL_WIDTH3_SHIFT 24U
// pixel width of plane 2 [1..48]
#define DMA_READ_PXL_WIDTH2_MASK 0x003F0000U
#define DMA_READ_PXL_WIDTH2_SHIFT 16U
// pixel width of plane 1 [1..48]
#define DMA_READ_PXL_WIDTH1_MASK 0x00003F00U
#define DMA_READ_PXL_WIDTH1_SHIFT 8U
// pixel width of plane 0 [1..48]
#define DMA_READ_PXL_WIDTH0_MASK 0x0000003FU
#define DMA_READ_PXL_WIDTH0_SHIFT 0U

/* DMA_ARUSER_REG */
// AXI write address user field plane 3
#define DMA_READ_ARUSER3_MASK 0xFF000000U
#define DMA_READ_ARUSER3_SHIFT 24U
// AXI write address user field plane 2
#define DMA_READ_ARUSER2_MASK 0x00FF0000U
#define DMA_READ_ARUSER2_SHIFT 16U
// AXI write address user field plane 1
#define DMA_READ_ARUSER1_MASK 0x0000FF00U
#define DMA_READ_ARUSER1_SHIFT 8U
// AXI write address user field plane 0
#define DMA_READ_ARUSER0_MASK 0x000000FFU
#define DMA_READ_ARUSER0_SHIFT 0U

/* DMA_READ_BA0_REG */
// * AXI master mode: (Base address) physical AXI address == [35:8] of picture plane 0 buffer
// --------------------------------------------------------------
// * AXI slave  mode: (Target address)   AXI address [11:0] is used as target address for plane 0.
//
// If transfer generating module is RPP_DMA_WRITE:
// the bit [7:0] are zero and bits [11:8] select the channel, since the base address programmed in RPP_DMA_WRITE [27:0] is mapped to address [35:0] with 8 lsb '0'
// --------------------------------------------------------------
// Programmed value becomes effective (visible in corresponding shadow register) after a forced software update
// or an automatic config update.
#define DMA_READ_BA0_MASK 0x0FFFFFFFU
#define DMA_READ_BA0_SHIFT 0U

/* DMA_READ_BA1_REG */
// * AXI master mode: (Base address) physical AXI address == [35:8] of picture plane 0 buffer
// --------------------------------------------------------------
// * AXI slave  mode: (Target address)   AXI address [11:0] is used as target address for plane 0.
//
// If transfer generating module is RPP_DMA_WRITE:
// the bit [7:0] are zero and bits [11:8] select the channel, since the base address programmed in RPP_DMA_WRITE [27:0] is mapped to address [35:0] with 8 lsb '0'
// --------------------------------------------------------------
// Programmed value becomes effective (visible in corresponding shadow register) after a forced software update
// or an automatic config update.
#define DMA_READ_BA1_MASK 0x0FFFFFFFU
#define DMA_READ_BA1_SHIFT 0U

/* DMA_READ_BA2_REG */
// * AXI master mode: (Base address) physical AXI address == [35:8] of picture plane 0 buffer
// --------------------------------------------------------------
// * AXI slave  mode: (Target address)   AXI address [11:0] is used as target address for plane 0.
//
// If transfer generating module is RPP_DMA_WRITE:
// the bit [7:0] are zero and bits [11:8] select the channel, since the base address programmed in RPP_DMA_WRITE [27:0] is mapped to address [35:0] with 8 lsb '0'
// --------------------------------------------------------------
// Programmed value becomes effective (visible in corresponding shadow register) after a forced software update
// or an automatic config update.
#define DMA_READ_BA2_MASK 0x0FFFFFFFU
#define DMA_READ_BA2_SHIFT 0U

/* DMA_READ_BA3_REG */
// * AXI master mode: (Base address) physical AXI address == [35:8] of picture plane 0 buffer
// --------------------------------------------------------------
// * AXI slave  mode: (Target address)   AXI address [11:0] is used as target address for plane 0.
//
// If transfer generating module is RPP_DMA_WRITE:
// the bit [7:0] are zero and bits [11:8] select the channel, since the base address programmed in RPP_DMA_WRITE [27:0] is mapped to address [35:0] with 8 lsb '0'
// --------------------------------------------------------------
// Programmed value becomes effective (visible in corresponding shadow register) after a forced software update
// or an automatic config update.
#define DMA_READ_BA3_MASK 0x0FFFFFFFU
#define DMA_READ_BA3_SHIFT 0U

/* DMA_READ_MEM_LINE_SIZE0_REG */
// Memory size for pixel line of buffer for plane 0
// The size is 256 byte aligned ==> physical size is dma_read_mem_line_size * 256
// Programmed value becomes effective (visible in corresponding shadow register) after a soft reset,
// a forced software update or an automatic config update.
#define DMA_READ_MEM_LINE_SIZE0_MASK 0x000003FFU
#define DMA_READ_MEM_LINE_SIZE0_SHIFT 0U

/* DMA_READ_MEM_LINE_SIZE1_REG */
// Memory size for pixel line of buffer for plane 1
// The size is 256 byte aligned ==> physical size is dma_read_mem_line_size * 256
// Programmed value becomes effective (visible in corresponding shadow register) after a soft reset,
// a forced software update or an automatic config update.
#define DMA_READ_MEM_LINE_SIZE1_MASK 0x000003FFU
#define DMA_READ_MEM_LINE_SIZE1_SHIFT 0U

/* DMA_READ_MEM_LINE_SIZE2_REG */
// Memory size for pixel line of buffer for plane 2
// The size is 256 byte aligned ==> physical size is dma_read_mem_line_size * 256
// Programmed value becomes effective (visible in corresponding shadow register) after a soft reset,
// a forced software update or an automatic config update.
#define DMA_READ_MEM_LINE_SIZE2_MASK 0x000003FFU
#define DMA_READ_MEM_LINE_SIZE2_SHIFT 0U

/* DMA_READ_MEM_LINE_SIZE3_REG */
// Memory size for pixel line of buffer for plane 3
// The size is 256 byte aligned ==> physical size is dma_read_mem_line_size * 256
// Programmed value becomes effective (visible in corresponding shadow register) after a soft reset,
// a forced software update or an automatic config update.
#define DMA_READ_MEM_LINE_SIZE3_MASK 0x000003FFU
#define DMA_READ_MEM_LINE_SIZE3_SHIFT 0U

/* DMA_READ_PIC_SIZE_REG */
// vertical picture size in pixel of component 0
// Programmed value becomes effective (visible in corresponding shadow register) after a forced software update
// or an automatic config update.
#define DMA_READ_V_SIZE_MASK 0x1FFF0000U
#define DMA_READ_V_SIZE_SHIFT 16U
// vertical picture size in pixel of component 0
// Programmed value becomes effective (visible in corresponding shadow register) after a forced software update
// or an automatic config update.
#define DMA_READ_H_SIZE_MASK 0x00003FFFU
#define DMA_READ_H_SIZE_SHIFT 0U

#endif /* __RPP_DMA_READ_REGS_H__ */
